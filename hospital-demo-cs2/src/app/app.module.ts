import { AppRoutesModule } from "./app.routes";
import { SharedModule } from "./shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { WaitComponent } from "./framework/wait/wait.component";
import { LoginComponent } from "./framework/login/login.component";
import { AppService } from "./app.service";
import { HospitalModule } from "./views/hospital/hospital.module";
import { DiagnosisModule } from "./views/diagnosis/diagnosis.module";
import { WardModule } from "./views/ward/ward.module";
import { PrescriptionModule } from "./views/prescription/prescription.module";
import { PatientModule } from "./views/patient/patient.module";
import { PrescriptionLineModule } from "./views/prescriptionline/prescriptionline.module";
import { MedicationModule } from "./views/medication/medication.module";
import { DoctorModule } from "./views/doctor/doctor.module";


@NgModule({
  declarations: [AppComponent, LoginComponent, WaitComponent],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HospitalModule,
    DiagnosisModule,
    WardModule,
    PrescriptionModule,
    PatientModule,
    PrescriptionLineModule,
    MedicationModule,
    DoctorModule,
    
    AppRoutesModule
  ],
  providers: [Location, AppService],
  bootstrap: [AppComponent]
})
export class AppModule {}