import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspWardsPerHospital2",
    templateUrl: "./dspWardsPerHospital2.component.html"
})
    
export class DspWardsPerHospital2Component implements OnInit {
    ward :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    country = [
					{value: '_USA', code: 'USA', description: 'USA - United States of America'},
			{value: '_RSA', code: 'RSA', description: 'RSA - South Africa'},
			{value: '_UK', code: 'UK', description: 'UK - United Kingdom'},
			{value: '_FRA', code: 'FRA', description: 'FRA - France'},
			{value: '_AUS', code: 'AUS', description: 'AUS - Australia'},
			{value: '_CAN', code: 'CAN', description: 'CAN - Canada'},
			{value: '_GER', code: 'GER', description: 'GER - Germany'},
			]
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Add", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"},
			{ id: "fKey-15", btnTitle: "Hospital", signal: "funKey15", display: true, cmdKey:"15"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: "DSP Wards per Hospital 2",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'wardCode', header: "Ward Code", isReadOnly: true},
				{field: 'wardName', header: "Ward Name", isReadOnly: true},
				{field: 'patientCount', header: "Patient Count", isReadOnly: true},
				{field: 'addedUser', header: "Added User", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.ward = this.client.getModel();
        this.dialogData.data = this.ward.pageDto.content;
    }

    dialogEvent($event) {
        this.ward = $event;
        this.mySelections = this.ward['mySelections'];
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages("info", "Please select a record ", "Please select a record ");
            return;
        }
        this.mySelections.forEach(routeParams => {
            routeParams.selected = selected;
            this.ward["gdo"] = routeParams;
            this.onSubmit()
        });
    }
    
    onSubmit() {
        this.client.reply();
    }
}
