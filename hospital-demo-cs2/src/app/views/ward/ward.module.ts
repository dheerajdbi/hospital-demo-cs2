
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { SelectWardComponent} from "./selectward/selectWard.component";
import { EditWardComponent} from "./editward/editWard.component";
import { DspWardsPerHospitalComponent} from "./dspwardsperhospital/dspWardsPerHospital.component";
import { DspWardsPerHospital2Component} from "./dspwardsperhospital2/dspWardsPerHospital2.component"; 
 
export const ROUTES: Routes = [
    {
        path:'selectWard',
        component: SelectWardComponent
    },
    {
        path:'editWard',
        component: EditWardComponent
    },
    {
        path:'dspWardsPerHospital',
        component: DspWardsPerHospitalComponent
    },
    {
        path:'dspWardsPerHospital2',
        component: DspWardsPerHospital2Component
    },];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
SelectWardComponent,
EditWardComponent,
DspWardsPerHospitalComponent,
DspWardsPerHospital2Component,
],
 exports: [
SelectWardComponent,
EditWardComponent,
DspWardsPerHospitalComponent,
DspWardsPerHospital2Component,
]
})
export class WardModule {}