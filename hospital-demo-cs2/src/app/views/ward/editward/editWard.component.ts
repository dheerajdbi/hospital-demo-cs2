import { GridPanelComponent } from './../../../framework/grid-panel/grid-panel.component';
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { Component, OnInit , ViewChild } from "@angular/core";
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from "../../../framework/services/messages.service";
import { ActivatedRoute } from "@angular/router";
import { deepEqual } from 'assert';
    
@Component({
    selector: "app-editWard",
    templateUrl: "./editWard.component.html"
})
    
export class EditWardComponent implements OnInit {
    @ViewChild('grid') grid: GridPanelComponent;
    ward: any;
    public mySelections: any[] = [];
    public loader = false;
    public mode: string;
    consts: {};
    public dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Add", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"},
			{ id: "fKey-15", btnTitle: "Hospital", signal: "funKey15", display: true, cmdKey:"15"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: 'Edit Ward',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            
            tablePaginator: true,
            data: [],
            
            columns: [
                {field: 'wardCode', header: "Ward Code", isFilter: true, editable: true},
				{field: 'wardName', header: "Ward Name", isFilter: true, editable: true},
            ]
        };
    }
 
	dialogEvent($event) {
	    if ($event) {
            $event.forEach((data, idx) => {
                data['selected'] = '1';
                this.mySelections[idx] = data;
            });
        }
        this.ward['mySelections'] = this.mySelections;
    }

	ngOnInit() {
        this.ward = this.client.getModel();
        this.dialogData.data = this.ward.pageDto.content;
        if (this.ward['programMode'] === 'ADD') {
            this.funcParams[2].display = true;
            this.funcParams[1].display = false;
        } else if (this.ward['programMode'] === 'CHG') {
            this.funcParams[2].display = false;
            this.funcParams[1].display = true;
        }
	}
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages("info", "Please select a record ", "Please select a record ");
            return;
        }
        this.mySelections.forEach(routeParams => {
            routeParams.selected = selected;
            this.ward["gdo"] = routeParams;
            this.onSubmit()
        });
    }
    
	onSubmit() {
	    this.client.reply();
	}
}
