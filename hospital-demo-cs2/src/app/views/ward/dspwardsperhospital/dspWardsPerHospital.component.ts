import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspWardsPerHospital",
    templateUrl: "./dspWardsPerHospital.component.html"
})
    
export class DspWardsPerHospitalComponent implements OnInit {
    ward :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-06", btnTitle: "Display", signal: "funKey06", display: true, cmdKey:"06"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: "DSP Wards per Hospital",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'wardCode', header: "Ward Code", isReadOnly: true},
				{field: 'wardName', header: "Ward Name", isReadOnly: true},
				{field: 'patientCount', header: "Patient Count", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.ward = this.client.getModel();
        this.dialogData.data = this.ward.pageDto.content;
    }

    dialogEvent($event) {
        this.ward = $event;
        this.mySelections = this.ward['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
