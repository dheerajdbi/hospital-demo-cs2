import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SelectWardComponent} from "./../selectward/selectWard.component";

describe('Component: SelectWardComponent', () => {
    let component: SelectWardComponent;
    let fixture: ComponentFixture<SelectWardComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [SelectWardComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(SelectWardComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `SelectWardComponent`',() => {
        expect(SelectWardComponent.name).toBe('SelectWardComponent');
    });

    it('should have a method called `constructor`', ()=>{
        expect(SelectWardComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', ()=>{
        expect(SelectWardComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', ()=>{
        expect(SelectWardComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', ()=>{
        expect(SelectWardComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `processGrid`', ()=>{
        expect(SelectWardComponent.prototype.processGrid).toBeDefined();
    });

    it('method `processGrid` should not be null', ()=>{
        expect(SelectWardComponent.prototype.processGrid).not.toBeNull();
    });

    it('method `onSubmit` should not be null', ()=>{
        expect(SelectWardComponent.prototype.onSubmit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', ()=>{
        expect(SelectWardComponent.prototype.onSubmit).toBeDefined();
    });
});