
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selectWard",
    templateUrl: "./selectWard.component.html"
})

export class SelectWardComponent implements OnInit {
    public ward: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'hospitalCode', header: "Hospital Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'wardCode', header: "Ward Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'wardName', header: "Ward Name", editable: true, isReadOnly: true},
        ];

        this.ward = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.ward.pageDto.content;
        this.totalRecords = this.ward.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.ward["page"] = event.first / event.rows;
      this.ward["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.ward[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.ward["gdo"] = null;
      this.ward["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.ward["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.ward["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.ward["pageDto"] = null;
    this.client.reply();
  }
}
