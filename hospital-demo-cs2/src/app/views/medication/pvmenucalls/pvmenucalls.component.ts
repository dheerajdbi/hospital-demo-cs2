
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";
    
@Component({
    selector: "app-pvMenuCalls",
    templateUrl: "./pvMenuCalls.component.html"
})
    
export class PvMenuCallsComponent implements OnInit {
    public medication= {};
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService) {
        this.medication = client.getModel();
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"}
		];
		this.searchParams = [

		];
    }

    ngOnInit() {
    }

    onSubmit() {
        this.client.reply();
    }
}
