import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfilPrescribedMedicat",
    templateUrl: "./dspfilPrescribedMedicat.component.html"
})
    
export class DspfilPrescribedMedicatComponent implements OnInit {
    medication :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
        this.dialogData = {
            header: "DSPFIL Prescribed Medicat",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'medicationDescription', header: "Medication Description", isReadOnly: true},
				{field: 'medicationUnit', header: "Medication Unit", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.medication = this.client.getModel();
        this.dialogData.data = this.medication.pageDto.content;
    }

    dialogEvent($event) {
        this.medication = $event;
        this.mySelections = this.medication['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
