import { GridPanelComponent } from './../../../framework/grid-panel/grid-panel.component';
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { Component, OnInit , ViewChild } from "@angular/core";
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from "../../../framework/services/messages.service";
import { ActivatedRoute } from "@angular/router";
import { deepEqual } from 'assert';
    
@Component({
    selector: "app-editMedication",
    templateUrl: "./editMedication.component.html"
})
    
export class EditMedicationComponent implements OnInit {
    @ViewChild('grid') grid: GridPanelComponent;
    medication: any;
    public mySelections: any[] = [];
    public loader = false;
    public mode: string;
    consts: {};
    public dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Change", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: 'Edit Medication',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            dropdownMatchingItem: 'medicationUnit',
            tablePaginator: true,
            data: [],
            dropDownItems: [
                { value: '_T', code: 'T', description: 'T - Tablets'},
                { value: '_M', code: 'M', description: 'M - Milliliter'},
                { value: '_L', code: 'L', description: 'L - Liter'},
                { value: '_C', code: 'C', description: 'C - Capsules'},
                { value: '_P', code: 'P', description: 'P - Pills'},
                { value: '_S', code: 'S', description: 'S - Teaspoon'},
                { value: '_N', code: 'N', description: 'N - Number'},
                { value: '_0', code: '0', description: '0 - Zero Used Error'}
			],
            columns: [
                {field: 'medicationCode', header: "Medication Code", isFilter: true, editable: true, isSearch: true},
				{field: 'medicationDescription', header: "Medication Description", isFilter: true, editable: true},
				{field: 'medicationUnit', header: "Medication Unit", isFilter: true, editable: true, isDropDown: true},
				{field: 'medicationUnitValue', header: "Medication unit value", editable: true, isReadOnly: true},
            ]
        };
    }
 
	dialogEvent($event) {
	    if ($event) {
            $event.forEach((data, idx) => {
                data['selected'] = '1';
                this.mySelections[idx] = data;
            });
        }
        this.medication['mySelections'] = this.mySelections;
    }

	ngOnInit() {
        this.medication = this.client.getModel();
        this.dialogData.data = this.medication.pageDto.content;
        if (this.medication['programMode'] === 'ADD') {
            this.funcParams[2].display = true;
            this.funcParams[1].display = false;
        } else if (this.medication['programMode'] === 'CHG') {
            this.funcParams[2].display = false;
            this.funcParams[1].display = true;
        }
	}
    
	onSubmit() {
	    this.client.reply();
	}
}
