
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { SelectMedicationComponent} from "./selectmedication/selectMedication.component";
import { EditMedicationComponent} from "./editmedication/editMedication.component";
import { PvMenuCallsComponent} from "./pvmenucalls/pvMenuCalls.component";
import { DspfilPrescribedMedicatComponent} from "./dspfilprescribedmedicat/dspfilPrescribedMedicat.component";
import { ErEditMedsTestIfEntryPanelComponent} from "./ereditmedstestif/erEditMedsTestIfEntryPanel.component";
import { ErEditMedsTestIfPanelComponent} from "./ereditmedstestif/erEditMedsTestIfPanel.component"; 
 
export const ROUTES: Routes = [
    {
        path:'selectMedication',
        component: SelectMedicationComponent
    },
    {
        path:'editMedication',
        component: EditMedicationComponent
    },
    {
        path:'pvMenuCalls',
        component: PvMenuCallsComponent
    },
    {
        path:'dspfilPrescribedMedicat',
        component: DspfilPrescribedMedicatComponent
    },
    {
        path:'erEditMedsTestIfEntryPanel',
        component: ErEditMedsTestIfEntryPanelComponent
    },
    
    {
        path:'erEditMedsTestIfPanel',
        component: ErEditMedsTestIfPanelComponent
    },
    ];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
SelectMedicationComponent,
EditMedicationComponent,
PvMenuCallsComponent,
DspfilPrescribedMedicatComponent,
ErEditMedsTestIfEntryPanelComponent,
ErEditMedsTestIfPanelComponent,
],
 exports: [
SelectMedicationComponent,
EditMedicationComponent,
PvMenuCallsComponent,
DspfilPrescribedMedicatComponent,
ErEditMedsTestIfEntryPanelComponent,
ErEditMedsTestIfPanelComponent,
]
})
export class MedicationModule {}