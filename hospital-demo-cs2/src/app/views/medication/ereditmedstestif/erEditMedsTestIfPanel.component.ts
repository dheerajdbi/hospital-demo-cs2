import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
    
@Component({
 
    selector: "app-erEditMedsTestIfPanel",
    templateUrl: "./erEditMedsTestIfPanel.component.html"
})
    
export class ErEditMedsTestIfPanelComponent implements OnInit {
    medication = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    medicationUnit = [
			{value: '_T', code: 'T', description: 'T - Tablets'},
			{value: '_M', code: 'M', description: 'M - Milliliter'},
			{value: '_L', code: 'L', description: 'L - Liter'},
			{value: '_C', code: 'C', description: 'C - Capsules'},
			{value: '_P', code: 'P', description: 'P - Pills'},
			{value: '_S', code: 'S', description: 'S - Teaspoon'},
			{value: '_N', code: 'N', description: 'N - Number'},
			{value: '_0', code: '0', description: '0 - Zero Used Error'},
	];

    constructor(private client: TerminalClientService) {
        this.medication = client.getModel();
		this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-11", btnTitle: "Delete", signal: "funKey11", display: true, cmdKey:"11"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
  		this.medication =  this.client.getModel();
        
    }

    onSubmit() {
    	this.medication["cmdKey"] = "00";
        this.client.reply();
    }
}