
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selectMedication",
    templateUrl: "./selectMedication.component.html"
})

export class SelectMedicationComponent implements OnInit {
    public medication: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'medicationCode', header: "Medication Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'medicationDescription', header: "Medication Description", editable: true, isReadOnly: true},
				{field: 'medicationUnit', header: "Medication Unit", editable: true, isDropDown: true, isReadOnly: true},
        ];

        this.medication = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.medication.pageDto.content;
        this.totalRecords = this.medication.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.medication["page"] = event.first / event.rows;
      this.medication["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.medication[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.medication["gdo"] = null;
      this.medication["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.medication["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.medication["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.medication["pageDto"] = null;
    this.client.reply();
  }
}
