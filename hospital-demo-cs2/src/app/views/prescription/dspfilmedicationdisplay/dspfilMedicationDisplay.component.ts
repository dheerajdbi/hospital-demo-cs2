import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfilMedicationDisplay",
    templateUrl: "./dspfilMedicationDisplay.component.html"
})
    
export class DspfilMedicationDisplayComponent implements OnInit {
    prescription :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: "DSPFIL Medication display",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'doctorCode', header: "Doctor Code", isReadOnly: true},
				{field: 'prescriptionCode', header: "Prescription Code", isReadOnly: true},
				{field: 'prescriptionDate', header: "Prescription Date", isReadOnly: true},
				{field: 'prescriptionTime', header: "Prescription Time", isReadOnly: true},
				{field: 'doctorNotes', header: "Doctor Notes", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.prescription = this.client.getModel();
        this.dialogData.data = this.prescription.pageDto.content;
    }

    dialogEvent($event) {
        this.prescription = $event;
        this.mySelections = this.prescription['mySelections'];
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages("info", "Please select a record ", "Please select a record ");
            return;
        }
        this.mySelections.forEach(routeParams => {
            routeParams.selected = selected;
            this.prescription["gdo"] = routeParams;
            this.onSubmit()
        });
    }
    
    onSubmit() {
        this.client.reply();
    }
}
