
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selectDrPrescriptions",
    templateUrl: "./selectDrPrescriptions.component.html"
})

export class SelectDrPrescriptionsComponent implements OnInit {
    public prescription: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'doctorCode', header: "Doctor Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'prescriptionCode', header: "Prescription Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'prescriptionDate', header: "Prescription Date", editable: true, isReadOnly: true, isSearch: true},
				{field: 'prescriptionTime', header: "Prescription Time", editable: true, isReadOnly: true, isSearch: true},
				{field: 'patientCode', header: "Patient Code", editable: true, isReadOnly: true},
				{field: 'doctorNotes', header: "Doctor Notes", editable: true, isReadOnly: true},
        ];

        this.prescription = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.prescription.pageDto.content;
        this.totalRecords = this.prescription.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.prescription["page"] = event.first / event.rows;
      this.prescription["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.prescription[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.prescription["gdo"] = null;
      this.prescription["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.prescription["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.prescription["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.prescription["pageDto"] = null;
    this.client.reply();
  }
}
