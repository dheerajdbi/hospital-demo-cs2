import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TrnEditPrescriptionsComponent } from "./../trneditprescriptions/trnEditPrescriptions.component";

describe('Component: TrnEditPrescriptionsComponent', () => {
    let component: TrnEditPrescriptionsComponent;
    let fixture: ComponentFixture<TrnEditPrescriptionsComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [TrnEditPrescriptionsComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(TrnEditPrescriptionsComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `TrnEditPrescriptionsComponent`',() => {
        expect(TrnEditPrescriptionsComponent.name).toBe('TrnEditPrescriptionsComponent');
    });

    it('should have a method called `constructor`', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `dialogEvent`', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.dialogEvent).toBeDefined();
    });

    it('method `dialogEvent` should not be null', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.dialogEvent).not.toBeNull();
    });

    it('should have a method called `onSubmit`', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', ()=>{
        expect(TrnEditPrescriptionsComponent.prototype.onSubmit).not.toBeNull();
    });
});