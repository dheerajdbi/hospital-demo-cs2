
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-trnEditPrescriptions",
    templateUrl: "./trnEditPrescriptions.component.html"
})
    
export class TrnEditPrescriptionsComponent implements OnInit {
    public prescription = {};
	gridVariable: any;
	totalRecords;
    displayNoSelection = false;
	mySelections = [];
    visible = true;
    cols = [];
	dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.prescription = client.getModel();
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Add", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-11", btnTitle: "Delete", signal: "funKey11", display: true, cmdKey:"11"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"},
			{ id: "fKey-15", btnTitle: "Hospital", signal: "funKey15", display: true, cmdKey:"15"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
        this.gridVariable = this.prescription["pageDto"].content;
        this.totalRecords = this.prescription["pageDto"].totalElements;
        this.dialogData = {
            header: "TRN Edit Prescriptions",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            totalRecords: this.totalRecords,
            tablePaginator: true,
            flatVariable: this.prescription,
            data: this.gridVariable,
            columns: [
                {field: 'medicationCode', header: "Medication Code", editable: true, openPopModel: true},
				{field: 'medicationDescription', header: "Medication Description", editable: true, isReadOnly: true, openPopModel: true},
				{field: 'prescriptionQuantity', header: "Prescription Quantity", editable: true},
            ]
        };
        if (this.prescription["gridMode"] === "ADD") {
            this.funcParams[2].display = true;
            this.funcParams[1].display = false;
            this.dialogData.tablePaginator = false;
            this.dialogData.selectionMode = "none";
        } else if (this.prescription["gridMode"] === "UPDATE") {
              this.funcParams[2].display = false;
              this.funcParams[1].display = true;
              this.dialogData.tablePaginator = true;
              this.dialogData.selectionMode = "mulitiple";
        }
    }

    dialogEvent(event) {
        if (event.openPopModel) {
            this.prescription["trackIndex"] = event.index;
            this.prescription["popupField"] = event.col;
            this.prescription["popupModel"] = event.openPopModel;
            this.prescription["mySelections"] = this.prescription["pageDto"].content;
            this.onSubmit();
        } else {
            if (event) {
                event.forEach((data, idx) => {
                    data["selected"] = "1";
                    this.mySelections[idx] = data;
                });
            }
        }
        this.prescription["mySelections"] = this.mySelections;
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages("info", "Please select a record ", "Please select a record ");
            return;
        }
        this.mySelections.forEach(routeParams => {
            routeParams.selected = selected;
            this.prescription["gdo"] = routeParams;
            this.onSubmit()
        });
    }
    
    onSubmit() {
        this.prescription["pageDto"] = null;
        this.client.reply();
    }
}
    