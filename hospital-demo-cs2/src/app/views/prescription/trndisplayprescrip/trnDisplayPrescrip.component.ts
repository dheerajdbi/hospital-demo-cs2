
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";

@Component({
    selector: "app-trnDisplayPrescrip",
    templateUrl: "./trnDisplayPrescrip.component.html"
})

export class TrnDisplayPrescripComponent implements OnInit {
    prescription: any;
    dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    mySelections = [];
    gridVariable: any;
	totalRecords;

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.prescription = client.getModel();
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-15", btnTitle: "Hospital", signal: "funKey15", display: true, cmdKey:"15"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
        this.gridVariable = this.prescription["pageDto"].content;
        this.totalRecords = this.prescription["pageDto"].totalElements;
        this.dialogData = {
            header: "TRN Display Prescrip",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            totalRecords: this.totalRecords,
            tablePaginator: true,
            flatVariable: this.prescription,
            data: this.gridVariable,
            columns: [
                {field: 'prescriptionLineNumber', header: "Prescription Line Number", editable: true, isReadOnly: true},
				{field: 'medicationCode', header: "Medication Code", editable: true, isReadOnly: true},
				{field: 'medicationDescription', header: "Medication Description", editable: true, isReadOnly: true},
				{field: 'prescriptionQuantity', header: "Prescription Quantity", editable: true, isReadOnly: true},
            ]
        };
    }

    dialogEvent(event) {
        this.prescription["mySelections"] = this.mySelections;
    }

    onSubmit() {
    	this.prescription["pageDto"] = null;
        this.client.reply();
    }
}
