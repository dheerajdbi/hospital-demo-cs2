
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { TrnEditPrescriptionsComponent} from "./trneditprescriptions/trnEditPrescriptions.component";
import { DspfilMedicationDisplayComponent} from "./dspfilmedicationdisplay/dspfilMedicationDisplay.component";
import { SelectDrPrescriptionsComponent} from "./selectdrprescriptions/selectDrPrescriptions.component"; 
 
export const ROUTES: Routes = [
    {
        path:'trnEditPrescriptions',
        component: TrnEditPrescriptionsComponent
    },
    {
        path:'dspfilMedicationDisplay',
        component: DspfilMedicationDisplayComponent
    },
    {
        path:'selectDrPrescriptions',
        component: SelectDrPrescriptionsComponent
    },];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
TrnEditPrescriptionsComponent,
DspfilMedicationDisplayComponent,
SelectDrPrescriptionsComponent,
],
 exports: [
TrnEditPrescriptionsComponent,
DspfilMedicationDisplayComponent,
SelectDrPrescriptionsComponent,
]
})
export class PrescriptionModule {}