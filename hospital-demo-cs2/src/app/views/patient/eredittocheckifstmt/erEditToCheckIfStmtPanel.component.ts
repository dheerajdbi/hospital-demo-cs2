import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
    
@Component({
 
    selector: "app-erEditToCheckIfStmtPanel",
    templateUrl: "./erEditToCheckIfStmtPanel.component.html"
})
    
export class ErEditToCheckIfStmtPanelComponent implements OnInit {
    patient = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    patientStatus = [
			{value: '_N', code: 'N', description: 'N - New'},
			{value: '_M', code: 'M', description: 'M - Recurring Monthly'},
			{value: '_T', code: 'T', description: 'T - To be Transferred'},
			{value: '_F', code: 'F', description: 'F - With Full Medical Aid'},
			{value: '_H', code: 'H', description: 'H - Hospital Plan only'},
			{value: '_Z', code: 'Z', description: 'Z - No Medical Aid'},
			{value: '_P', code: 'P', description: 'P - Private Paid Upfront'},
	];

    constructor(private client: TerminalClientService) {
        this.patient = client.getModel();
		this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-11", btnTitle: "Delete", signal: "funKey11", display: true, cmdKey:"11"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
  		this.patient =  this.client.getModel();
        
    }

    onSubmit() {
    	this.patient["cmdKey"] = "00";
        this.client.reply();
    }
}