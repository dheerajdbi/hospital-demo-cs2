
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { SelectPatientComponent} from "./selectpatient/selectPatient.component";
import { EdrEditPatientDetailEntryPanelComponent} from "./edreditpatientdetail/edrEditPatientDetailEntryPanel.component";
import { EdrEditPatientDetailPanelComponent} from "./edreditpatientdetail/edrEditPatientDetailPanel.component";
import { DspfDisplayPatientsWrdComponent} from "./dspfdisplaypatientswrd/dspfDisplayPatientsWrd.component";
import { PmtDiagnosisAndPrescripComponent} from "./pmtdiagnosisandprescrip/pmtDiagnosisAndPrescrip.component";
import { DspfDisplayPatientWrd2Component} from "./dspfdisplaypatientwrd2/dspfDisplayPatientWrd2.component";
import { DspPatientsPerHospitalComponent} from "./dsppatientsperhospital/dspPatientsPerHospital.component";
import { ErEditToCheckIfStmtEntryPanelComponent} from "./eredittocheckifstmt/erEditToCheckIfStmtEntryPanel.component";
import { ErEditToCheckIfStmtPanelComponent} from "./eredittocheckifstmt/erEditToCheckIfStmtPanel.component";
import { EdrEditPatientDetailREntryPanelComponent} from "./edreditpatientdetailr/edrEditPatientDetailREntryPanel.component";
import { EdrEditPatientDetailRPanelComponent} from "./edreditpatientdetailr/edrEditPatientDetailRPanel.component"; 
 
export const ROUTES: Routes = [
    {
        path:'selectPatient',
        component: SelectPatientComponent
    },
    {
        path:'edrEditPatientDetailEntryPanel',
        component: EdrEditPatientDetailEntryPanelComponent
    },
    
    {
        path:'edrEditPatientDetailPanel',
        component: EdrEditPatientDetailPanelComponent
    },
    
    {
        path:'dspfDisplayPatientsWrd',
        component: DspfDisplayPatientsWrdComponent
    },
    {
        path:'pmtDiagnosisAndPrescrip',
        component: PmtDiagnosisAndPrescripComponent
    },
    {
        path:'dspfDisplayPatientWrd2',
        component: DspfDisplayPatientWrd2Component
    },
    {
        path:'dspPatientsPerHospital',
        component: DspPatientsPerHospitalComponent
    },
    {
        path:'erEditToCheckIfStmtEntryPanel',
        component: ErEditToCheckIfStmtEntryPanelComponent
    },
    
    {
        path:'erEditToCheckIfStmtPanel',
        component: ErEditToCheckIfStmtPanelComponent
    },
    
    {
        path:'edrEditPatientDetailREntryPanel',
        component: EdrEditPatientDetailREntryPanelComponent
    },
    
    {
        path:'edrEditPatientDetailRPanel',
        component: EdrEditPatientDetailRPanelComponent
    },
    ];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
SelectPatientComponent,
EdrEditPatientDetailEntryPanelComponent,
EdrEditPatientDetailPanelComponent,
DspfDisplayPatientsWrdComponent,
PmtDiagnosisAndPrescripComponent,
DspfDisplayPatientWrd2Component,
DspPatientsPerHospitalComponent,
ErEditToCheckIfStmtEntryPanelComponent,
ErEditToCheckIfStmtPanelComponent,
EdrEditPatientDetailREntryPanelComponent,
EdrEditPatientDetailRPanelComponent,
],
 exports: [
SelectPatientComponent,
EdrEditPatientDetailEntryPanelComponent,
EdrEditPatientDetailPanelComponent,
DspfDisplayPatientsWrdComponent,
PmtDiagnosisAndPrescripComponent,
DspfDisplayPatientWrd2Component,
DspPatientsPerHospitalComponent,
ErEditToCheckIfStmtEntryPanelComponent,
ErEditToCheckIfStmtPanelComponent,
EdrEditPatientDetailREntryPanelComponent,
EdrEditPatientDetailRPanelComponent,
]
})
export class PatientModule {}