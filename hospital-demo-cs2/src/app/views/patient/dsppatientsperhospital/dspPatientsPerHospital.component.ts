import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspPatientsPerHospital",
    templateUrl: "./dspPatientsPerHospital.component.html"
})
    
export class DspPatientsPerHospitalComponent implements OnInit {
    patient :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    country = [
					{value: '_USA', code: 'USA', description: 'USA - United States of America'},
			{value: '_RSA', code: 'RSA', description: 'RSA - South Africa'},
			{value: '_UK', code: 'UK', description: 'UK - United Kingdom'},
			{value: '_FRA', code: 'FRA', description: 'FRA - France'},
			{value: '_AUS', code: 'AUS', description: 'AUS - Australia'},
			{value: '_CAN', code: 'CAN', description: 'CAN - Canada'},
			{value: '_GER', code: 'GER', description: 'GER - Germany'},
			]
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: "DSP Patients per Hospital",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'wardName', header: "Ward Name", isReadOnly: true},
				{field: 'patientCode', header: "Patient Code", isReadOnly: true},
				{field: 'patientName', header: "Patient Name", isReadOnly: true},
				{field: 'patientSurname', header: "Patient Surname", isReadOnly: true},
				{field: 'patientGender', header: "Patient Gender", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.patient = this.client.getModel();
        this.dialogData.data = this.patient.pageDto.content;
    }

    dialogEvent($event) {
        this.patient = $event;
        this.mySelections = this.patient['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
