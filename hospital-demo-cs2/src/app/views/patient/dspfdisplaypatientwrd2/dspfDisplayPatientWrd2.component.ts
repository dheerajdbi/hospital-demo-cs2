import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfDisplayPatientWrd2",
    templateUrl: "./dspfDisplayPatientWrd2.component.html"
})
    
export class DspfDisplayPatientWrd2Component implements OnInit {
    patient :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        
        this.dialogData = {
            header: "DSPF Display Patient/Wrd2",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'patientCode', header: "Patient Code", isReadOnly: true},
				{field: 'patientName', header: "Patient Name", isReadOnly: true},
				{field: 'patientSurname', header: "Patient Surname", isReadOnly: true},
				{field: 'patientGender', header: "Patient Gender", isReadOnly: true},
				{field: 'patientStatus', header: "Patient Status", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.patient = this.client.getModel();
        this.dialogData.data = this.patient.pageDto.content;
    }

    dialogEvent($event) {
        this.patient = $event;
        this.mySelections = this.patient['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
