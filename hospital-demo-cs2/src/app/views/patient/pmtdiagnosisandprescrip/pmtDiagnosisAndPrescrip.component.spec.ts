import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PmtDiagnosisAndPrescripComponent } from "./../pmtdiagnosisandprescrip/pmtDiagnosisAndPrescrip.component";

describe('Component: PmtDiagnosisAndPrescripComponent', () => {
    let component: PmtDiagnosisAndPrescripComponent;
    let fixture: ComponentFixture<PmtDiagnosisAndPrescripComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [PmtDiagnosisAndPrescripComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PmtDiagnosisAndPrescripComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
     });

    it('should be named `PmtDiagnosisAndPrescripComponent`', () => {
        expect(PmtDiagnosisAndPrescripComponent.name).toBe("PmtDiagnosisAndPrescripComponent");
    });

    it('should have a method called `constructor`', () => {
        expect(PmtDiagnosisAndPrescripComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(PmtDiagnosisAndPrescripComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(PmtDiagnosisAndPrescripComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(PmtDiagnosisAndPrescripComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(PmtDiagnosisAndPrescripComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', () => {
        expect(PmtDiagnosisAndPrescripComponent.prototype.onSubmit).not.toBeNull();
    });
});