
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selectPatient",
    templateUrl: "./selectPatient.component.html"
})

export class SelectPatientComponent implements OnInit {
    public patient: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'patientCode', header: "Patient Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'wardCode', header: "Ward Code", editable: true, isReadOnly: true},
				{field: 'patientName', header: "Patient Name", editable: true, isReadOnly: true},
				{field: 'patientSurname', header: "Patient Surname", editable: true, isReadOnly: true},
        ];

        this.patient = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.patient.pageDto.content;
        this.totalRecords = this.patient.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.patient["page"] = event.first / event.rows;
      this.patient["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.patient[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.patient["gdo"] = null;
      this.patient["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.patient["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.patient["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.patient["pageDto"] = null;
    this.client.reply();
  }
}
