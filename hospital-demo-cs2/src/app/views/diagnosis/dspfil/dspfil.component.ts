import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfil",
    templateUrl: "./dspfil.component.html"
})
    
export class DspfilComponent implements OnInit {
    diagnosis :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"},
			{ id: "fKey-15", btnTitle: "Hopital", signal: "funKey15", display: true, cmdKey:"15"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
        this.dialogData = {
            header: "DSPFIL",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'diagnosisDate', header: "Diagnosis Date", isReadOnly: true, isSearch: true},
				{field: 'diagnosisTime', header: "Diagnosis Time", isReadOnly: true},
				{field: 'doctorCode', header: "Doctor Code", isReadOnly: true, isSearch: true},
				{field: 'doctorName', header: "Doctor Name", isReadOnly: true},
				{field: 'findings1', header: "Findings 1", isReadOnly: true},
				{field: 'findings2', header: "Findings 2", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.diagnosis = this.client.getModel();
        this.dialogData.data = this.diagnosis.pageDto.content;
    }

    dialogEvent($event) {
        this.diagnosis = $event;
        this.mySelections = this.diagnosis['mySelections'];
    }
    
    process(selected) {
        if (!this.mySelections || this.mySelections.length === 0) {
            this.msgService.pushToMessages("info", "Please select a record ", "Please select a record ");
            return;
        }
        this.mySelections.forEach(routeParams => {
            routeParams.selected = selected;
            this.diagnosis["gdo"] = routeParams;
            this.onSubmit()
        });
    }
    
    onSubmit() {
        this.client.reply();
    }
}
