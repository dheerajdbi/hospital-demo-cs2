
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selSelectDiagnosis",
    templateUrl: "./selSelectDiagnosis.component.html"
})

export class SelSelectDiagnosisComponent implements OnInit {
    public diagnosis: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'diagnosisDate', header: "Diagnosis Date", editable: true, isReadOnly: true, isSearch: true},
				{field: 'diagnosisTime', header: "Diagnosis Time", editable: true, isReadOnly: true, isSearch: true},
				{field: 'doctorCode', header: "Doctor Code", editable: true, isReadOnly: true},
				{field: 'doctorName', header: "Doctor Name", editable: true, isReadOnly: true},
				{field: 'findings1', header: "Findings 1", editable: true, isReadOnly: true},
				{field: 'findings2', header: "Findings 2", editable: true, isReadOnly: true},
        ];

        this.diagnosis = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.diagnosis.pageDto.content;
        this.totalRecords = this.diagnosis.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.diagnosis["page"] = event.first / event.rows;
      this.diagnosis["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.diagnosis[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.diagnosis["gdo"] = null;
      this.diagnosis["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.diagnosis["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.diagnosis["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.diagnosis["pageDto"] = null;
    this.client.reply();
  }
}
