
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { DspfilComponent} from "./dspfil/dspfil.component";
import { EdtrcdEditDiagnosisEntryPanelComponent} from "./edtrcdeditdiagnosis/edtrcdEditDiagnosisEntryPanel.component";
import { EdtrcdEditDiagnosisPanelComponent} from "./edtrcdeditdiagnosis/edtrcdEditDiagnosisPanel.component";
import { SelSelectDiagnosisComponent} from "./selselectdiagnosis/selSelectDiagnosis.component"; 
 
export const ROUTES: Routes = [
    {
        path:'dspfil',
        component: DspfilComponent
    },
    {
        path:'edtrcdEditDiagnosisEntryPanel',
        component: EdtrcdEditDiagnosisEntryPanelComponent
    },
    
    {
        path:'edtrcdEditDiagnosisPanel',
        component: EdtrcdEditDiagnosisPanelComponent
    },
    
    {
        path:'selSelectDiagnosis',
        component: SelSelectDiagnosisComponent
    },];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
DspfilComponent,
EdtrcdEditDiagnosisEntryPanelComponent,
EdtrcdEditDiagnosisPanelComponent,
SelSelectDiagnosisComponent,
],
 exports: [
DspfilComponent,
EdtrcdEditDiagnosisEntryPanelComponent,
EdtrcdEditDiagnosisPanelComponent,
SelSelectDiagnosisComponent,
]
})
export class DiagnosisModule {}