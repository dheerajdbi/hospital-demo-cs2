import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
    
@Component({
 
    selector: "app-edtrcdEditDiagnosisPanel",
    templateUrl: "./edtrcdEditDiagnosisPanel.component.html"
})
    
export class EdtrcdEditDiagnosisPanelComponent implements OnInit {
    diagnosis = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    

    constructor(private client: TerminalClientService) {
        this.diagnosis = client.getModel();
		this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-11", btnTitle: "Delete", signal: "funKey11", display: true, cmdKey:"11"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"},
			{ id: "fKey-14", btnTitle: "Load", signal: "funKey14", display: true, cmdKey:"14"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
  		this.diagnosis =  this.client.getModel();
        
    }

    onSubmit() {
    	this.diagnosis["cmdKey"] = "00";
        this.client.reply();
    }
}