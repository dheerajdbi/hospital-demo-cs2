import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
    
@Component({
 
    selector: "app-edtrcdEditDiagnosisEntryPanel",
    templateUrl: "./edtrcdEditDiagnosisEntryPanel.component.html"
})
    
export class EdtrcdEditDiagnosisEntryPanelComponent implements OnInit {
    diagnosis = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    

    constructor(private client: TerminalClientService) {
        this.diagnosis = client.getModel();
		this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Add", signal: "funKey09", display: true, cmdKey:"09"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
  		this.diagnosis =  this.client.getModel();
        if (this.diagnosis["patientCode"] && this.diagnosis["diagnosisDate"] && this.diagnosis["diagnosisTime"]) {
			this.onSubmit();
		}
    }

    onSubmit() {
    	this.diagnosis["cmdKey"] = "00";
        this.client.reply();
    }
}