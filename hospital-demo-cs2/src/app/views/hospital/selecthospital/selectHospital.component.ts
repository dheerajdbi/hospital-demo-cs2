
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selectHospital",
    templateUrl: "./selectHospital.component.html"
})

export class SelectHospitalComponent implements OnInit {
    public hospital: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'hospitalCode', header: "Hospital Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'hospitalName', header: "Hospital Name", editable: true, isReadOnly: true},
        ];

        this.hospital = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.hospital.pageDto.content;
        this.totalRecords = this.hospital.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.hospital["page"] = event.first / event.rows;
      this.hospital["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.hospital[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.hospital["gdo"] = null;
      this.hospital["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.hospital["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.hospital["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.hospital["pageDto"] = null;
    this.client.reply();
  }
}
