import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SelectHospitalComponent} from "./../selecthospital/selectHospital.component";

describe('Component: SelectHospitalComponent', () => {
    let component: SelectHospitalComponent;
    let fixture: ComponentFixture<SelectHospitalComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [SelectHospitalComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(SelectHospitalComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `SelectHospitalComponent`',() => {
        expect(SelectHospitalComponent.name).toBe('SelectHospitalComponent');
    });

    it('should have a method called `constructor`', ()=>{
        expect(SelectHospitalComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', ()=>{
        expect(SelectHospitalComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', ()=>{
        expect(SelectHospitalComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', ()=>{
        expect(SelectHospitalComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `processGrid`', ()=>{
        expect(SelectHospitalComponent.prototype.processGrid).toBeDefined();
    });

    it('method `processGrid` should not be null', ()=>{
        expect(SelectHospitalComponent.prototype.processGrid).not.toBeNull();
    });

    it('method `onSubmit` should not be null', ()=>{
        expect(SelectHospitalComponent.prototype.onSubmit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', ()=>{
        expect(SelectHospitalComponent.prototype.onSubmit).toBeDefined();
    });
});