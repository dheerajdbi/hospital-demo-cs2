import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TerminalClientState } from "./../../../framework/services/terminal-client.service";
import { BehaviorSubject } from "rxjs/Rx";
import { ErEditHospitalChangeEntryPanelComponent } from "./../eredithospitalchange/erEditHospitalChangeEntryPanel.component";

describe('Component: ErEditHospitalChangeEntryPanelComponent', () => {
    let component: ErEditHospitalChangeEntryPanelComponent;
    let fixture: ComponentFixture<ErEditHospitalChangeEntryPanelComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [ErEditHospitalChangeEntryPanelComponent],
            providers: [
                Logger,
                {
                    provide: TerminalClientService,
                    useValue: {
                        reply: jasmine.createSpy(null, null),
                        setSignal: jasmine.createSpy(null, null),
                        getModel: jasmine.createSpy(null, null).and.returnValue({
                        }),
                    state: new BehaviorSubject(TerminalClientState.INITIALIZING)
                    }
                },
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ErEditHospitalChangeEntryPanelComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should be named `ErEditHospitalChangeEntryPanelComponent`',() => {
        expect(ErEditHospitalChangeEntryPanelComponent.name).toBe('ErEditHospitalChangeEntryPanelComponent');
    });

    it('should have a method called `constructor`', ()=>{
        expect(ErEditHospitalChangeEntryPanelComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', ()=>{
        expect(ErEditHospitalChangeEntryPanelComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', ()=>{
        expect(ErEditHospitalChangeEntryPanelComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', ()=>{
        expect(ErEditHospitalChangeEntryPanelComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', ()=>{
    expect(ErEditHospitalChangeEntryPanelComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', ()=>{
        expect(ErEditHospitalChangeEntryPanelComponent.prototype.onSubmit).not.toBeNull();
    });
});