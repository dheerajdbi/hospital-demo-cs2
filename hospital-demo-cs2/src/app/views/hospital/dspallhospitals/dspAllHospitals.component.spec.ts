import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TerminalClientState } from "./../../../framework/services/terminal-client.service";
import { BehaviorSubject } from "rxjs/Rx";
import { DspAllHospitalsComponent } from "./../dspallhospitals/dspAllHospitals.component";

describe('Component: DspAllHospitalsComponent', () => {
    let component: DspAllHospitalsComponent;
    let fixture: ComponentFixture<DspAllHospitalsComponent>;
  
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [DspAllHospitalsComponent],
            providers: [
                Logger,
                {
                    provide: TerminalClientService,
                    useValue: {
                        reply: jasmine.createSpy(null, null),
                        setSignal: jasmine.createSpy(null, null),
                        getModel: jasmine.createSpy(null, null).and.returnValue({
                            //TODO:We have to place respective json data in gdoList
                            gdoList: [{
                                diagnosisDate: "18-07-29",
                                diagnosisTime: "01:24",
                                doctorCode: 1,
                                doctorName: "Tester",
                                findings1: "abc",
                                findings2: "def",
                            }]
                        }),
                    state: new BehaviorSubject(TerminalClientState.INITIALIZING)
                    }
                },
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));
  
    beforeEach(() => {
        fixture = TestBed.createComponent(DspAllHospitalsComponent);
        component = fixture.componentInstance;
    });
  
    it('should create', () => {
        expect(component).toBeTruthy();
    });
 
    it('should be named `DspAllHospitalsComponent`',() => {
        expect(DspAllHospitalsComponent.name).toBe('DspAllHospitalsComponent');
    });

    it('should have a method called `ngOnInit`', ()=>{
        expect(DspAllHospitalsComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', ()=>{
        expect(DspAllHospitalsComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `dialogEvent`', ()=>{
        expect(DspAllHospitalsComponent.prototype.dialogEvent).toBeDefined();
    });

    it('method `dialogEvent` should not be null', ()=>{
        expect(DspAllHospitalsComponent.prototype.dialogEvent).not.toBeNull();
    });

    it('should have a method called `onSubmit`', ()=>{
        expect(DspAllHospitalsComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', ()=>{
        expect(DspAllHospitalsComponent.prototype.onSubmit).not.toBeNull();
    });
});