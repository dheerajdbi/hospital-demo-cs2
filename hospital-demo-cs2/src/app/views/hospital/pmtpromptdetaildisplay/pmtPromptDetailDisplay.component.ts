
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";
    
@Component({
    selector: "app-pmtPromptDetailDisplay",
    templateUrl: "./pmtPromptDetailDisplay.component.html"
})
    
export class PmtPromptDetailDisplayComponent implements OnInit {
    public hospital= {};
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService) {
        this.hospital = client.getModel();
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
    }

    onSubmit() {
        this.client.reply();
    }
}
