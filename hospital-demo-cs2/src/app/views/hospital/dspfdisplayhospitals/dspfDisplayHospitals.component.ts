import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfDisplayHospitals",
    templateUrl: "./dspfDisplayHospitals.component.html"
})
    
export class DspfDisplayHospitalsComponent implements OnInit {
    hospital :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    country = [
					{value: '_USA', code: 'USA', description: 'USA - United States of America'},
			{value: '_RSA', code: 'RSA', description: 'RSA - South Africa'},
			{value: '_UK', code: 'UK', description: 'UK - United Kingdom'},
			{value: '_FRA', code: 'FRA', description: 'FRA - France'},
			{value: '_AUS', code: 'AUS', description: 'AUS - Australia'},
			{value: '_CAN', code: 'CAN', description: 'CAN - Canada'},
			{value: '_GER', code: 'GER', description: 'GER - Germany'},
			]
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
        this.dialogData = {
            header: "DSPF Display Hospitals",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'hospitalName', header: "Hospital Name", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.hospital = this.client.getModel();
        this.dialogData.data = this.hospital.pageDto.content;
    }

    dialogEvent($event) {
        this.hospital = $event;
        this.mySelections = this.hospital['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
