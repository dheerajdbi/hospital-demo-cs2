import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
    
@Component({
 
    selector: "app-erEditHospitalPanel",
    templateUrl: "./erEditHospitalPanel.component.html"
})
    
export class ErEditHospitalPanelComponent implements OnInit {
    hospital = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    country = [
			{value: '_USA', code: 'USA', description: 'USA - United States of America'},
			{value: '_RSA', code: 'RSA', description: 'RSA - South Africa'},
			{value: '_UK', code: 'UK', description: 'UK - United Kingdom'},
			{value: '_FRA', code: 'FRA', description: 'FRA - France'},
			{value: '_AUS', code: 'AUS', description: 'AUS - Australia'},
			{value: '_CAN', code: 'CAN', description: 'CAN - Canada'},
			{value: '_GER', code: 'GER', description: 'GER - Germany'},
	];

    constructor(private client: TerminalClientService) {
        this.hospital = client.getModel();
		this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-06", btnTitle: "Wards", signal: "funKey06", display: true, cmdKey:"06"},
			{ id: "fKey-09", btnTitle: "Patients", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-10", btnTitle: "Doctors", signal: "funKey10", display: true, cmdKey:"10"},
			{ id: "fKey-11", btnTitle: "Delete", signal: "funKey11", display: true, cmdKey:"11"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
  		this.hospital =  this.client.getModel();
        
    }

    onSubmit() {
    	this.hospital["cmdKey"] = "00";
        this.client.reply();
    }
}