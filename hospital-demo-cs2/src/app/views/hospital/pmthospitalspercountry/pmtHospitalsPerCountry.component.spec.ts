import { SharedModule } from "./../../../shared.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule, APP_BASE_HREF } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed, async } from "@angular/core/testing";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { Logger } from '@nsalaun/ng-logger';
import { GrowlModule } from 'primeng/primeng';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PmtHospitalsPerCountryComponent } from "./../pmthospitalspercountry/pmtHospitalsPerCountry.component";

describe('Component: PmtHospitalsPerCountryComponent', () => {
    let component: PmtHospitalsPerCountryComponent;
    let fixture: ComponentFixture<PmtHospitalsPerCountryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                RouterModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                SharedModule,FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule
            ],
            declarations: [PmtHospitalsPerCountryComponent],
            providers: [
                Logger,
                TerminalClientService,
                {
                    provide: APP_BASE_HREF, useValue: "/"
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PmtHospitalsPerCountryComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
     });

    it('should be named `PmtHospitalsPerCountryComponent`', () => {
        expect(PmtHospitalsPerCountryComponent.name).toBe("PmtHospitalsPerCountryComponent");
    });

    it('should have a method called `constructor`', () => {
        expect(PmtHospitalsPerCountryComponent.prototype.constructor).toBeDefined();
    });

    it('method `constructor` should not be null', () => {
        expect(PmtHospitalsPerCountryComponent.prototype.constructor).not.toBeNull();
    });

    it('should have a method called `ngOnInit`', () => {
        expect(PmtHospitalsPerCountryComponent.prototype.ngOnInit).toBeDefined();
    });

    it('method `ngOnInit` should not be null', () => {
        expect(PmtHospitalsPerCountryComponent.prototype.ngOnInit).not.toBeNull();
    });

    it('should have a method called `onSubmit`', () => {
        expect(PmtHospitalsPerCountryComponent.prototype.onSubmit).toBeDefined();
    });

    it('method `onSubmit` should not be null', () => {
        expect(PmtHospitalsPerCountryComponent.prototype.onSubmit).not.toBeNull();
    });
});