
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { SelectHospitalComponent} from "./selecthospital/selectHospital.component";
import { ErEditHospitalEntryPanelComponent} from "./eredithospital/erEditHospitalEntryPanel.component";
import { ErEditHospitalPanelComponent} from "./eredithospital/erEditHospitalPanel.component";
import { PmtPromptDetailDisplayComponent} from "./pmtpromptdetaildisplay/pmtPromptDetailDisplay.component";
import { PmtHospitalsPerCountryComponent} from "./pmthospitalspercountry/pmtHospitalsPerCountry.component";
import { DspfDisplayHospitalsComponent} from "./dspfdisplayhospitals/dspfDisplayHospitals.component";
import { DspAllHospitalsComponent} from "./dspallhospitals/dspAllHospitals.component";
import { ErEditHospitalChangeEntryPanelComponent} from "./eredithospitalchange/erEditHospitalChangeEntryPanel.component";
import { ErEditHospitalChangePanelComponent} from "./eredithospitalchange/erEditHospitalChangePanel.component";
import { PmtTestBatchFunctionComponent} from "./pmttestbatchfunction/pmtTestBatchFunction.component"; 
 
export const ROUTES: Routes = [
    {
        path:'selectHospital',
        component: SelectHospitalComponent
    },
    {
        path:'erEditHospitalEntryPanel',
        component: ErEditHospitalEntryPanelComponent
    },
    
    {
        path:'erEditHospitalPanel',
        component: ErEditHospitalPanelComponent
    },
    
    {
        path:'pmtPromptDetailDisplay',
        component: PmtPromptDetailDisplayComponent
    },
    {
        path:'pmtHospitalsPerCountry',
        component: PmtHospitalsPerCountryComponent
    },
    {
        path:'dspfDisplayHospitals',
        component: DspfDisplayHospitalsComponent
    },
    {
        path:'dspAllHospitals',
        component: DspAllHospitalsComponent
    },
    {
        path:'erEditHospitalChangeEntryPanel',
        component: ErEditHospitalChangeEntryPanelComponent
    },
    
    {
        path:'erEditHospitalChangePanel',
        component: ErEditHospitalChangePanelComponent
    },
    
    {
        path:'pmtTestBatchFunction',
        component: PmtTestBatchFunctionComponent
    },];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
SelectHospitalComponent,
ErEditHospitalEntryPanelComponent,
ErEditHospitalPanelComponent,
PmtPromptDetailDisplayComponent,
PmtHospitalsPerCountryComponent,
DspfDisplayHospitalsComponent,
DspAllHospitalsComponent,
ErEditHospitalChangeEntryPanelComponent,
ErEditHospitalChangePanelComponent,
PmtTestBatchFunctionComponent,
],
 exports: [
SelectHospitalComponent,
ErEditHospitalEntryPanelComponent,
ErEditHospitalPanelComponent,
PmtPromptDetailDisplayComponent,
PmtHospitalsPerCountryComponent,
DspfDisplayHospitalsComponent,
DspAllHospitalsComponent,
ErEditHospitalChangeEntryPanelComponent,
ErEditHospitalChangePanelComponent,
PmtTestBatchFunctionComponent,
]
})
export class HospitalModule {}