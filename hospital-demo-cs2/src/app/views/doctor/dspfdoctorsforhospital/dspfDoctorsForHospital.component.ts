import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfDoctorsForHospital",
    templateUrl: "./dspfDoctorsForHospital.component.html"
})
    
export class DspfDoctorsForHospitalComponent implements OnInit {
    doctor :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    country = [
					{value: '_USA', code: 'USA', description: 'USA - United States of America'},
			{value: '_RSA', code: 'RSA', description: 'RSA - South Africa'},
			{value: '_UK', code: 'UK', description: 'UK - United Kingdom'},
			{value: '_FRA', code: 'FRA', description: 'FRA - France'},
			{value: '_AUS', code: 'AUS', description: 'AUS - Australia'},
			{value: '_CAN', code: 'CAN', description: 'CAN - Canada'},
			{value: '_GER', code: 'GER', description: 'GER - Germany'},
			]
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Add", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: "DSPF Doctors for Hospital",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'doctorCode', header: "Doctor Code", isReadOnly: true},
				{field: 'doctorName', header: "Doctor Name", isReadOnly: true},
				{field: 'doctorContactNumber', header: "Doctor Contact Number", isReadOnly: true},
				{field: 'specialityLevel', header: "Speciality Level", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.doctor = this.client.getModel();
        this.dialogData.data = this.doctor.pageDto.content;
    }

    dialogEvent($event) {
        this.doctor = $event;
        this.mySelections = this.doctor['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
