import { GridPanelComponent } from './../../../framework/grid-panel/grid-panel.component';
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { Component, OnInit , ViewChild } from "@angular/core";
import { TerminalClientService } from '../../../framework/services/terminal-client.service';
import { MessagesService } from "../../../framework/services/messages.service";
import { ActivatedRoute } from "@angular/router";
import { deepEqual } from 'assert';
    
@Component({
    selector: "app-editDoctorHospital",
    templateUrl: "./editDoctorHospital.component.html"
})
    
export class EditDoctorHospitalComponent implements OnInit {
    @ViewChild('grid') grid: GridPanelComponent;
    doctor: any;
    public mySelections: any[] = [];
    public loader = false;
    public mode: string;
    consts: {};
    public dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Change", signal: "funKey09", display: true, cmdKey:"09"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
        this.dialogData = {
            header: 'Edit Doctor Hospital',
            onEscapeOption: true,
            selectionMode: 'multiple',
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            dropdownMatchingItem: 'specialityLevel',
            tablePaginator: true,
            data: [],
            dropDownItems: [
                { value: '_G', code: 'G', description: 'G - Professor General'},
                { value: '_O', code: 'O', description: 'O - Professor Orthopedics'},
                { value: '_D', code: 'D', description: 'D - Professor Dentistrys'},
                { value: '_1', code: '1', description: '1 - General Doctor'},
                { value: '_2', code: '2', description: '2 - General Dentist'}
			],
            columns: [
                {field: 'hospitalCode', header: "Hospital Code", editable: true, isReadOnly: true, openPopModel: true},
				{field: 'hospitalName', header: "Hospital Name", editable: true, isReadOnly: true, openPopModel: true},
				{field: 'doctorCode', header: "Doctor Code", isFilter: true, editable: true, isSearch: true},
				{field: 'doctorName', header: "Doctor Name", isFilter: true, editable: true},
				{field: 'doctorContactNumber', header: "Doctor Contact Number", isFilter: true, editable: true},
				{field: 'specialityLevel', header: "Speciality Level", isFilter: true, editable: true, isDropDown: true},
				{field: 'supervisingDoctorDoctor', header: "Supervising Doctor Doctor", isFilter: true, editable: true},
				{field: 'supervisingDoctorName', header: "Supervising Doctor Name", editable: true, isReadOnly: true},
            ]
        };
    }
 
	dialogEvent($event) {
	    if ($event) {
            $event.forEach((data, idx) => {
                data['selected'] = '1';
                this.mySelections[idx] = data;
            });
        }
        this.doctor['mySelections'] = this.mySelections;
    }

	ngOnInit() {
        this.doctor = this.client.getModel();
        this.dialogData.data = this.doctor.pageDto.content;
        if (this.doctor['programMode'] === 'ADD') {
            this.funcParams[2].display = true;
            this.funcParams[1].display = false;
        } else if (this.doctor['programMode'] === 'CHG') {
            this.funcParams[2].display = false;
            this.funcParams[1].display = true;
        }
	}
    
	onSubmit() {
	    this.client.reply();
	}
}
