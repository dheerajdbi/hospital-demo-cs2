import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
    
@Component({
 
    selector: "app-editDoctorRcdEntryPanel",
    templateUrl: "./editDoctorRcdEntryPanel.component.html"
})
    
export class EditDoctorRcdEntryPanelComponent implements OnInit {
    doctor = {};
    dialogData: any;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];
    alertDialogDisplay = false;

    specialityLevel = [
			{value: '_G', code: 'G', description: 'G - Professor General'},
			{value: '_O', code: 'O', description: 'O - Professor Orthopedics'},
			{value: '_D', code: 'D', description: 'D - Professor Dentistrys'},
			{value: '_1', code: '1', description: '1 - General Doctor'},
			{value: '_2', code: '2', description: '2 - General Dentist'},
	];

    constructor(private client: TerminalClientService) {
        this.doctor = client.getModel();
		this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-09", btnTitle: "Change", signal: "funKey09", display: true, cmdKey:"09"}
		];
		this.searchParams = [
			{ id: "fKey-04", btnTitle: "Prompt", signal: "funKey04", display: true, cmdKey:"04"}
		];
    }

    ngOnInit() {
  		this.doctor =  this.client.getModel();
        if (this.doctor["doctorCode"]) {
			this.onSubmit();
		}
    }

    onSubmit() {
    	this.doctor["cmdKey"] = "00";
        this.client.reply();
    }
}