
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { SelectDoctorComponent} from "./selectdoctor/selectDoctor.component";
import { EditDoctorComponent} from "./editdoctor/editDoctor.component";
import { DspDoctorDetailEntryPanelComponent} from "./dspdoctordetail/dspDoctorDetailEntryPanel.component";
import { DspDoctorDetailPanelComponent} from "./dspdoctordetail/dspDoctorDetailPanel.component";
import { DspfDoctorsForHospitalComponent} from "./dspfdoctorsforhospital/dspfDoctorsForHospital.component";
import { EditDoctorRcdEntryPanelComponent} from "./editdoctorrcd/editDoctorRcdEntryPanel.component";
import { EditDoctorRcdPanelComponent} from "./editdoctorrcd/editDoctorRcdPanel.component";
import { EditDoctorHospitalComponent} from "./editdoctorhospital/editDoctorHospital.component"; 
 
export const ROUTES: Routes = [
    {
        path:'selectDoctor',
        component: SelectDoctorComponent
    },
    {
        path:'editDoctor',
        component: EditDoctorComponent
    },
    {
        path:'dspDoctorDetailEntryPanel',
        component: DspDoctorDetailEntryPanelComponent
    },
    
    {
        path:'dspDoctorDetailPanel',
        component: DspDoctorDetailPanelComponent
    },
    
    {
        path:'dspfDoctorsForHospital',
        component: DspfDoctorsForHospitalComponent
    },
    {
        path:'editDoctorRcdEntryPanel',
        component: EditDoctorRcdEntryPanelComponent
    },
    
    {
        path:'editDoctorRcdPanel',
        component: EditDoctorRcdPanelComponent
    },
    
    {
        path:'editDoctorHospital',
        component: EditDoctorHospitalComponent
    },];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
SelectDoctorComponent,
EditDoctorComponent,
DspDoctorDetailEntryPanelComponent,
DspDoctorDetailPanelComponent,
DspfDoctorsForHospitalComponent,
EditDoctorRcdEntryPanelComponent,
EditDoctorRcdPanelComponent,
EditDoctorHospitalComponent,
],
 exports: [
SelectDoctorComponent,
EditDoctorComponent,
DspDoctorDetailEntryPanelComponent,
DspDoctorDetailPanelComponent,
DspfDoctorsForHospitalComponent,
EditDoctorRcdEntryPanelComponent,
EditDoctorRcdPanelComponent,
EditDoctorHospitalComponent,
]
})
export class DoctorModule {}