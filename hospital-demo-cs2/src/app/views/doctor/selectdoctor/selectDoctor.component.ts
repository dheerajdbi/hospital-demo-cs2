
import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService, TerminalClientState } from "../../../framework/services/terminal-client.service";


@Component({
    selector: "app-selectDoctor",
    templateUrl: "./selectDoctor.component.html"
})

export class SelectDoctorComponent implements OnInit {
    public doctor: any;
    public funcParams: FunctionKey[];
    gridVariable: any;
    totalRecords;
    displayNoSelection = false;
    mySelections = [];
    visible = true;
    cols = [];
    init: boolean = false;

    constructor(private client: TerminalClientService) {
        this.funcParams = [
          { id: "fKey-01", btnTitle: "Ok", signal: "funKey01", display: true },
          { id: "fKey-03", btnTitle: "Cancel", signal: "funKey03", display: true }
        ];

        this.cols = [
            {field: 'doctorCode', header: "Doctor Code", editable: true, isReadOnly: true, isSearch: true},
				{field: 'doctorName', header: "Doctor Name", editable: true, isReadOnly: true},
				{field: 'doctorContactNumber', header: "Doctor Contact Number", editable: true, isReadOnly: true},
				{field: 'specialityLevel', header: "Speciality Level", editable: true, isDropDown: true, isReadOnly: true},
				{field: 'hospitalCode', header: "Hospital Code", editable: true, isReadOnly: true},
				{field: 'supervisingDoctorDoctor', header: "Supervising Doctor Doctor", editable: true, isReadOnly: true},
				{field: 'supervisingDoctorName', header: "Supervising Doctor Name", editable: true, isReadOnly: true},
        ];

        this.doctor = client.getModel();
    }

    ngOnInit() {
        this.gridVariable = this.doctor.pageDto.content;
        this.totalRecords = this.doctor.pageDto.totalElements;
    }

    processGrid(event) {
    if (this.init) {
      this.doctor["page"] = event.first / event.rows;
      this.doctor["size"] = event.rows;
      if (event.sortField !== undefined) {
        this.doctor[event.sortField] = event.sortOrder == 1 ? "ASC" : "DESC";
      }
      this.doctor["gdo"] = null;
      this.doctor["pageDto"] = null;
      this.onSubmit(3);
    } else {
      this.init = true;
    }
  }

   onSubmit(val) {
    if (
      this.mySelections !== undefined &&
      this.mySelections !== null &&
      this.mySelections &&
      val == 1
    ) {
      this.doctor["gdo"] = this.mySelections;
    }
    if (val == 2) {
      this.doctor["gdo"] = null;
      this.displayNoSelection = true;
    }
    this.doctor["pageDto"] = null;
    this.client.reply();
  }
}
