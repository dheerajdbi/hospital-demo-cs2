
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { SharedModule } from "../../shared.module";
import { Routes, RouterModule } from "@angular/router";

import { DspfilMediactionComponent} from "./dspfilmediaction/dspfilMediaction.component"; 
 
export const ROUTES: Routes = [
    {
        path:'dspfilMediaction',
        component: DspfilMediactionComponent
    },];
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
 declarations: [
DspfilMediactionComponent,
],
 exports: [
DspfilMediactionComponent,
]
})
export class PrescriptionLineModule {}