import { Component, OnInit } from "@angular/core";
import { FunctionKey } from "./../../../framework/function-key-panel/function-key-panel.component";
import { TerminalClientService } from "../../../framework/services/terminal-client.service";
import { MessagesService } from "../../../framework/services/messages.service";
    
@Component({
    selector: "app-dspfilMediaction",
    templateUrl: "./dspfilMediaction.component.html"
})
    
export class DspfilMediactionComponent implements OnInit {
    prescriptionLine :any;
    public mySelections: any[];
    public dialogData;
    public searchParams: FunctionKey[];
    public funcParams: FunctionKey[];

    
    constructor(private client: TerminalClientService, private msgService: MessagesService) {
        this.funcParams = [
			{ id: "fKey-03", btnTitle: "Exit", signal: "funKey03", display: true, cmdKey:"03"},
			{ id: "fKey-12", btnTitle: "Cancel", signal: "funKey12", display: true, cmdKey:"12"}
		];
		this.searchParams = [

		];
        this.dialogData = {
            header: "Dspfil Mediaction",
            onEscapeOption: true,
            selectionMode: "multiple",
            isDownload: false,
            selectionOption: false,
            rowsDisplayed: 5,
            tablePaginator: true,
            data: [],
            columns: [
                {field: 'prescriptionLineNumber', header: "Prescription Line Number", isReadOnly: true},
				{field: 'medicationCode', header: "Medication Code", isReadOnly: true},
				{field: 'prescriptionQuantity', header: "Prescription Quantity", isReadOnly: true},
				{field: 'medicationDescription', header: "Medication Description", isReadOnly: true},
				{field: 'medicationUnit', header: "Medication Unit", isReadOnly: true},
				{field: 'medicationUnitValue', header: "Medication unit value", isReadOnly: true},
            ]
        };
    }

    ngOnInit() {
        this.prescriptionLine = this.client.getModel();
        this.dialogData.data = this.prescriptionLine.pageDto.content;
    }

    dialogEvent($event) {
        this.prescriptionLine = $event;
        this.mySelections = this.prescriptionLine['mySelections'];
    }
    
    onSubmit() {
        this.client.reply();
    }
}
