import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { GlobalVariable } from './framework/services/global-variables';
import { LoaderService } from './framework/loader/loader.service';
import { environment } from '../environments/environment';

@Injectable()
export class AppDataService {

    private wsUrl: string;
    private headers: HttpHeaders;
    private pgmParams: HttpParams;

    constructor(private http: HttpClient, private globalVariable: GlobalVariable,
        private loaderService: LoaderService) {
        this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        this.pgmParams = new HttpParams();
    }

    /**
     * dynamically form the url from nextStep
     */
    private getWebServiceUrl() {
        const baseUrl = environment.apiUrl;

        if (this.globalVariable.$flatVariable != null && this.globalVariable.$flatVariable !== undefined) {
            const programName = this.globalVariable.$flatVariable.nextStep.program.split('.').pop().replace('Controller', '');
            const packageName = this.globalVariable.$flatVariable.nextStep.program.split('.').splice(3, 1);

            return this.globalVariable.$wsUrl =
                baseUrl + packageName + '/' + programName + '/' + this.globalVariable.$flatVariable.nextStep.step;
        } else if (this.globalVariable.$authenticated === undefined) {
            return baseUrl + 'authentication/user';
        }
    }

    /**
     * extract pathParam from pathParams and form url
     */
    private extractPathParams(pathParams?: any[]) {
        if (pathParams && pathParams !== undefined) {
            if (typeof pathParams[0] === 'string' && pathParams[0].toLowerCase().indexOf('menu') !== -1) {
                this.wsUrl = environment.apiUrl;
            } else {
                this.wsUrl = this.wsUrl.concat('/');
            }

            for (let i = 0; i < pathParams.length; i++) {
                this.wsUrl = this.wsUrl.concat(pathParams[i]);

                if (i !== pathParams.length - 1) {
                    this.wsUrl = this.wsUrl.concat('/');
                }
            }
        }
    }

    /**
     * extract queryParam from queryParams and send to HTTP Verb
     */
    private extractQueryParams(queryParams?: any): HttpParams {
        if (queryParams && queryParams !== undefined) {
            Object.keys(queryParams)
                .filter(key => {
                    const value = queryParams[key];
                    return (Array.isArray(value) || typeof value === 'string') ?
                     (value.length > 0) : (value !== null && value !== undefined);
                })
                .forEach(key => {
                    this.pgmParams = this.pgmParams.set(key, queryParams[key]);
                });
        }

        return this.pgmParams;
    }

    /**
     * Webservice methods to handle all HTTP Verbs for all component
     */
    public get(pathParams?: any[], queryParams?: any, header?: HttpHeaders) {
        this.wsUrl = this.getWebServiceUrl();
        this.extractPathParams(pathParams);

        this.loaderService.show();
        return this.http.get(this.wsUrl, { headers: header, params: this.extractQueryParams(queryParams) })
            .map(res => res);
    }

    public save(pathParams?: string, queryParams?: string): Observable<any> {
        this.loaderService.show();
        return this.http.post(this.getWebServiceUrl(), { params: this.pgmParams })
            .map(res => res);
    }

    public update(flatVariable: any, pathParams?: any[], queryParams?: any): Observable<any> {
        this.wsUrl = this.getWebServiceUrl();
        this.extractPathParams(pathParams);

        this.loaderService.show();
        return this.http.put(this.getWebServiceUrl(), flatVariable, { params: this.extractQueryParams(queryParams) })
            .map(res => res);
    }

    public patch(flatVariable: any, pathParams?: any[], queryParams?: any): Observable<any> {
        this.wsUrl = this.getWebServiceUrl();
        this.extractPathParams(pathParams);

        this.loaderService.show();
        return this.http.patch(this.getWebServiceUrl(), flatVariable, { params: this.extractQueryParams(queryParams) })
            .map(res => res);
    }

    public delete(pathParams?: any[], queryParams?: any): Observable<any> {
        this.wsUrl = this.getWebServiceUrl();
        this.extractPathParams(pathParams);

        this.loaderService.show();
        return this.http.delete(this.getWebServiceUrl(), { params: this.extractQueryParams(queryParams) })
            .map(res => res);
    }
}
