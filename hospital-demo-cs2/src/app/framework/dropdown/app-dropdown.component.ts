import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-dropdown',
  templateUrl: './app-dropdown.component.html',
})
export class AppDropdownComponent {
  selectedOne: SelectItem;
  @Input() dropdownParams: DropdownParams;
  @Output() changeEvent = new EventEmitter();
  change() {
    this.changeEvent.emit(this.selectedOne);
  }
}
/**
 * usage: <app-dropdown [dropdownParams]="params" (changeEvent)="change($event)"></app-dropdown>
 */
export interface DropdownParams {
  doptions: SelectItem[]; // An array of objects to display as the available options.
  placeholder?: string; // Default text to display when no option is selected.
  disableFlag?: boolean; // When present, it specifies that the component should be disabled.
  editableFlag?: boolean; // When present, custom value instead of predefined options can be entered using the editable input field.
  width?: string; // custom width of dropdown
  color?: string; // color of dropdown
}
