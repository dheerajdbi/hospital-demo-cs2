import { Directive, OnInit, ElementRef } from '@angular/core';
import { Input, HostListener } from '@angular/core';
import { TerminalClientService, TerminalClientState } from '../services/terminal-client.service';
import { Logger } from '@nsalaun/ng-logger';
export const KEYCODE_TO_FUNCTION_KEY = {
  '112': 'fKey-01', '113': 'fKey-02', '114': 'fKey-03', '115': 'fKey-04', '116': 'fKey-05', '117': 'fKey-06',
  '118': 'fKey-07', '119': 'fKey-08', '120': 'fKey-09', '121': 'fKey-10', '122': 'fKey-11', '123': 'fKey-12',
  '112-shift': 'fKey-13', '113-shift': 'fKey-14', '114-shift': 'fKey-15', '115-shift': 'fKey-16',
  '116-shift': 'fKey-17', '117-shift': 'fKey-18', '118-shift': 'fKey-19', '119-shift': 'fKey-20',
  '120-shift': 'fKey-21', '121-shift': 'fKey-22', '122-shift': 'fKey-23', '123-shift': 'fKey-24',
};

@Directive({
  selector: '[appFunKey]'
})
export class FunKeyDirective implements OnInit {

  @Input()
  appFunKey: string;


  constructor(private el: ElementRef, private logger: Logger) {
  }

  ngOnInit() { }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    const keyCode = event.keyCode || event.which;
    if (event.shiftKey && KEYCODE_TO_FUNCTION_KEY[keyCode] !== undefined
      && KEYCODE_TO_FUNCTION_KEY[keyCode + '-shift'] === this.appFunKey) {
      event.preventDefault();
      this.el.nativeElement.click();
    } else if (!event.shiftKey && KEYCODE_TO_FUNCTION_KEY[keyCode] !== undefined && KEYCODE_TO_FUNCTION_KEY[keyCode] === this.appFunKey) {
      event.preventDefault();
      this.el.nativeElement.click();
    }
  }
}
