import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
    selector: '[appCapitalize]'
})

export class CapitalizeDirective {

    constructor(private el: ElementRef) {
    }

    @HostListener('keyup') onKeyUp() {
        this.el.nativeElement.value = this.el.nativeElement.value.toUpperCase();
    }
}
