import { TestBed, inject } from '@angular/core/testing';
import { ElementRef } from '@angular/core';
import { RightAlignDirective } from './right-align.directive';

describe('RightAlignDirective', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
            provide: ElementRef,
            useValue: {
              nativeElement: {
                    style: {
                        textAlign : {}
                    }
              }
            }
          },
      ]
    });
  });

  describe('#constructor', () => {
    it('should create an instance', inject([ElementRef], (el) => {
      const directive = new RightAlignDirective(el);
      expect(directive).toBeDefined();
    }));
    it('should assign text-align of native element to right', inject([ElementRef], (el) => {
        spyOn(el.nativeElement.style, 'textAlign');
        const directive = new RightAlignDirective(el);
        expect(el.nativeElement.style.textAlign).toEqual('right');
      }));
  });

});
