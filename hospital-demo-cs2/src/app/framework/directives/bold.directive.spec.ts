import { TestBed, inject } from '@angular/core/testing';
import { ElementRef } from '@angular/core';
import { BoldDirective } from './bold.directive';

describe('BoldDirective', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
            provide: ElementRef,
            useValue: {
              nativeElement: {
                    style: {
                        fontWeight : {}
                    }
              }
            }
          },
      ]
    });
  });

  describe('#constructor', () => {
    it('should create an instance', inject([ElementRef], (el) => {
      const directive = new BoldDirective(el);
      expect(directive).toBeDefined();
    }));
    it('should assign font-weight of native element to bold', inject([ElementRef], (el) => {
        spyOn(el.nativeElement.style, 'fontWeight');
        const directive = new BoldDirective(el);
        expect(el.nativeElement.style.fontWeight).toEqual('bold');
      }));
  });

});
