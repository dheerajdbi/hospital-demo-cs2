import { Directive, OnInit, ElementRef, Input } from '@angular/core';

@Directive({
    selector: '[appAlert]'
})
/*
Alert Options:
Danger, Primary, Secondary, Success, Info, Light, Dark
*/
export class AlertDirective implements OnInit {

    @Input() appAlert: string;


    constructor(private el: ElementRef) { }

    ngOnInit() {
        this.appAlert = this.appAlert.toLowerCase();
        switch (this.appAlert) {
            case null || undefined || 'danger':
                this.el.nativeElement.className = 'alert alert-danger';
                break;
            case 'primary':
                this.el.nativeElement.className = 'alert alert-primary';
                break;
            case 'secondary':
                this.el.nativeElement.className = 'alert alert-secondary';
                break;
            case 'success':
                this.el.nativeElement.className = 'alert alert-success';
                break;
            case 'info':
                this.el.nativeElement.className = 'alert alert-info';
                break;
            case 'light':
                this.el.nativeElement.className = 'alert alert-light';
                break;
            case 'dark':
                this.el.nativeElement.className = 'alert alert-dark';
                break;
            default:
                this.el.nativeElement.className = 'alert alert-danger';
                break;
        }
    }


}
