import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[appOnlyNumber]'
})

// <input id="user" type="text" class="input" [(ngModel)]='model.username' onlyNumber>
export class OnlynumberDirective {
    constructor(private el: ElementRef) {
    }

    @HostListener('keyup') onKeyUp(evt) {
        if ((evt.which < 46 || evt.which > 57) || evt.which === 47) {
            evt.preventDefault();
        }
    }
}
