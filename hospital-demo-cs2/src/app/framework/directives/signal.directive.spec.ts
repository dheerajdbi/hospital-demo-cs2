/*
import { SignalDirective } from './signal.directive';
import {TerminalClientState, TerminalClientService } from '../services/terminal-client.service';
import { TestBed, inject } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpHeaders, HttpHandler } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Logger } from '@nsalaun/ng-logger';

describe('SignalDirective', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: TerminalClientService,
          useValue: {
            reply: jasmine.createSpy("",null),
            setSignal: jasmine.createSpy("",null),
            state: new BehaviorSubject(TerminalClientState.INITIALIZING)
          }
        },
        {
          provide: Logger,
          useValue: {
            debug: jasmine.createSpy("",null),
            warn: jasmine.createSpy("",null)
          }
        },
        HttpClient,
        HttpHandler
      ]
    });
  });

  describe('#constructor', () => {
    it('should create an instance', inject([TerminalClientService, Logger], (client, logger) => {
      const directive = new SignalDirective(client, logger);
      expect(directive).toBeDefined();
    }));
  });

  describe('#ngOnInit', () => {

    it('should reset signal and log some debug info', inject([TerminalClientService, Logger], (client, logger) => {
      const directive = new SignalDirective(client, logger);
      const signal = 'funKey03';
      directive.appSignal = signal;

      directive.ngOnInit();

      expect(client.setSignal).toHaveBeenCalledWith(signal, false);
      expect(logger.debug).toHaveBeenCalled();

    }));

    it('should do nothing if no signal is bound', inject([TerminalClientService, Logger], (client, logger) => {
      const directive = new SignalDirective(client, logger);

      directive.ngOnInit();

      expect(client.setSignal).not.toHaveBeenCalled();
      expect(logger.debug).not.toHaveBeenCalled();

    }));

  });

  describe('#onClick', () => {
    it('should warn that terminal is locked and abort signal', inject([TerminalClientService, Logger], (client, logger) => {
      const directive = new SignalDirective(client, logger);
      directive.appSignal = 'funKey03';

      const lockedState = [
        TerminalClientState.INITIALIZING,
        TerminalClientState.NOT_AUTHENTICATED,
        TerminalClientState.NOT_CONNECTED,
        TerminalClientState.SENDING_REPLY,
        TerminalClientState.STALLED,
        TerminalClientState.WAITING_REQUEST
      ];

      for (const currentState of lockedState) {

        // Reset the spies
        client.setSignal.calls.reset();
        logger.warn.calls.reset();

        client.state.next(currentState);
        directive.onClick({/** I am a button! });
        expect(client.setSignal).not.toHaveBeenCalled();
        expect(logger.warn).toHaveBeenCalled();
      }
    }));

    it('should send signal if state is ON_SCREEN', inject([TerminalClientService, Logger], (client, logger) => {
      const directive = new SignalDirective(client, logger);
      const signal = 'funKey03';
      directive.appSignal = signal;
      client.state.next(TerminalClientState.ON_SCREEN);
      directive.onClick({/** I am a button! });
      expect(client.setSignal).toHaveBeenCalledWith(signal, true);
      expect(client.reply).toHaveBeenCalled();
      //expect(client.setSignal).toHaveBeenCalledBefore(client.reply);
      expect(logger.debug).toHaveBeenCalled();
    }));

    it('should do nothing if signal is not set', inject([TerminalClientService, Logger], (client, logger) => {
      const directive = new SignalDirective(client, logger);
      expect(client.setSignal).not.toHaveBeenCalled();
      expect(client.reply).not.toHaveBeenCalled();
      expect(logger.debug).not.toHaveBeenCalled();
      expect(logger.warn).not.toHaveBeenCalled();
    }));

  });
});
*/