import { Component, OnInit, HostListener } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
  currentURL: string;
  constructor(private router: Router, private location: Location ) {
    this.currentURL = this.router.url;
  }
  goToMainMenu() {
    this.router.navigate(['MainMenu']);
  }
  goToPrevious() {
    this.location.back();
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvents(event: KeyboardEvent) {
    const keyCode = event.keyCode || event.which;
    if (keyCode === 112) {
      event.preventDefault();
      event.stopPropagation();
      event.returnValue = false;
      this.goToPrevious();
    } else if (keyCode === 113) {
      event.preventDefault();
      event.stopPropagation();
      event.returnValue = false;
      this.goToMainMenu();
    }
  }

  ngOnInit() {
  }

}
