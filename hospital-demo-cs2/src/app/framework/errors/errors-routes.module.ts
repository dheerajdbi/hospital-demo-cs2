import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';



export const ROUTES: Routes = [
  { path: 'error/404', component: PageNotFoundComponent },

];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, { enableTracing: true }),
  ],
  exports: [RouterModule]
})
export class ErrorsRoutesModule { }
