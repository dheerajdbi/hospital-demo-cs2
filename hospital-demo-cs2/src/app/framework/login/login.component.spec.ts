import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { MockBackend } from '@angular/http/testing';
import { HttpModule } from '@angular/http';
import { AuthService } from '../services/auth.service';
import { Logger } from '@nsalaun/ng-logger';
import { SharedModule } from '../../shared.module';
import { GrowlModule } from 'primeng/primeng';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,ReactiveFormsModule,HttpClientTestingModule, GrowlModule, RouterTestingModule],
      declarations: [LoginComponent],
      providers: [Logger, AuthService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

it('should be named `LoginComponent`',() => {
  expect(LoginComponent.name).toBe('LoginComponent');
   });

it('should have a method called `constructor`', ()=>{
    expect(LoginComponent.prototype.constructor).toBeDefined();
});
 
it('method `constructor` should not be null', ()=>{
    expect(LoginComponent.prototype.constructor).not.toBeNull();
});

it('should have a method called `ngOnit`', ()=>{
    expect(LoginComponent.prototype.ngOnInit).toBeDefined();
});

it('should have a method called `ngOnit`', ()=>{
    expect(LoginComponent.prototype.ngOnInit).toBeDefined();
});

it('should have a method called `login`', ()=>{
  expect(LoginComponent.prototype.login).toBeDefined();
  });
   
  it('method `login` should not be null', ()=>{
      expect(LoginComponent.prototype.login).not.toBeNull();
  });

});
