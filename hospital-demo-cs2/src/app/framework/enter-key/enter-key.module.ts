import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, RouterLink } from '@angular/router';
import { EnterKeyComponent } from './enter-key.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    EnterKeyComponent,
  ],
  exports: [ EnterKeyComponent ]
})
export class EnterKeyModule { }
