import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppDataService } from '../../app.data.service';
import { HttpErrorResponse } from '@angular/common/http/src/response';

@Component({
  selector: 'app-grid-panel',
  templateUrl: './grid-panel.component.html'
})
export class GridPanelComponent {
  flatVariable = {};
  private totalRecords: any;
  mySelections: any[];
  selectDropDownItem: any[] = [];
  copyDropDownItem: any[] = [];
  @Input()
  data: {
    header: string;
    onEscapeOption: boolean; // Handle hide function .
    selectionMode?: string;
    data: any;
    totalRecords: number;
    endPoint: any[];
    width?: string;
    height?: string;
    rowsDisplayed?: number;
    tablePaginator?: boolean;
    selectionOption?: boolean;
    isDownload?: boolean;
    dropDownItems?: any[];
    dropdownMatchingItem?: string;
    columns: TableParam[];
    buttons: PopUpButton[];
    flatVariable?: {};
  };
  @Output() dialogEvent = new EventEmitter();

  onClick($event, signal) {
    this.dialogEvent.emit({ $event, signal });
  }

  hide() {
    if (this.data.onEscapeOption) {
      // this.submit();
    }
  }

  getPage(event: LazyLoadEvent) {
    this.flatVariable['mySelections'] = this.mySelections;
    if (this.data.dropDownItems) {
      this.data.data.forEach((rowData, id) => {
        this.data.dropDownItems.forEach(dropDownItem => {
          if (rowData[this.data.dropdownMatchingItem] === dropDownItem.code) {
            this.selectDropDownItem[id] = dropDownItem;
          }
        });
      }, this);
    }
    this.dialogEvent.emit(this.mySelections);
  }

  performFilter(flatVariable) {
    this.data.flatVariable = flatVariable;
    this.getPage(null);
  }

  onRowSelect($event) {
    this.dialogEvent.emit(this.mySelections);
  }

  onRowUnselect($event) {
    this.dialogEvent.emit(this.mySelections);
  }

  setFilterData(filterValue: string, filterColumnName: string) {
    this.flatVariable[filterColumnName] = filterValue;
  }

  changeValue(selected, index, col) {
    this.data.data[index][col] = selected.code;
  }

  openPopModel(index, column) {
    this.dialogEvent.emit({ openPopModel: true, index: index, col: column });
  }
}

interface TableParam {
  field: string;
  header: string;
  editable?: boolean;
  style?: string;
  joinColumn: boolean;
  joinColumnsList?: any;
  hideColumn?: boolean;
  isSearch?: boolean;
  isFilter?: boolean;
  isReadOnly?: boolean;
  isDropDown?: boolean;
  openPopModel?: boolean;
}

interface PopUpButton {
  label: string;
  icon: string;
}
