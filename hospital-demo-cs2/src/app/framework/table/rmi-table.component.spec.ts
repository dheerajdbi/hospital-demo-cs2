import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RMITableComponent } from './rmi-table.component';
import { CommonModule } from '@angular/common';
import { TableModule, Table } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { Paginator } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventEmitter } from '@angular/core';
import { MessagesService } from '../services/messages.service';

describe('RMITableComponent', () => {
  let component: RMITableComponent;
  let fixture: ComponentFixture<RMITableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        TableModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [RMITableComponent],
      providers: [MessagesService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RMITableComponent);
    component = fixture.componentInstance;
    component.options = {
      'selectionMode': 'single',
      'selection': null,
      'selectedIndex': -1,
      'paginator': true,
      'rows': '10',
      'reorderableColumns': true,
      'rowPerPageOptions': '[10, 20, 30]',
      'loading': true,
      'summary': '1=Update | 4=Delete | 5=View'
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Should prevent default and call paginate', () => {
    it('On pageup', () => {
      const keyboardPressEvent: any = document.createEvent('CustomEvent');
      keyboardPressEvent.which = 33;
      spyOn(component, 'paginate');
      spyOn(keyboardPressEvent, 'preventDefault');
      keyboardPressEvent.initEvent('keydown', true, true);
      component.handleKeyboardEvents(keyboardPressEvent);
      expect(keyboardPressEvent.preventDefault).toHaveBeenCalled();
      expect(component.paginate).toHaveBeenCalledWith('previous');
    });
    it('On pagedown', () => {
      const keyboardPressEvent: any = document.createEvent('CustomEvent');
      keyboardPressEvent.which = 34;
      spyOn(component, 'paginate');
      spyOn(keyboardPressEvent, 'preventDefault');
      keyboardPressEvent.initEvent('keydown', true, true);
      component.handleKeyboardEvents(keyboardPressEvent);
      expect(keyboardPressEvent.preventDefault).toHaveBeenCalled();
      expect(component.paginate).toHaveBeenCalledWith('next');
    });
  });

  describe('Paginate', () => {
    it('should paginate a page backwards on previous', () => {
      spyOn(component, 'turboTable').and.returnValue({
        totalRecords: 20,
        rows: 10,
        first: 10
      });
      component.paginate('previous');
      expect(component.turboTable.first).toEqual(0);
    });
    it('should paginate a page forwards on next', () => {
      spyOn(component, 'turboTable').and.returnValue({
        totalRecords: 20,
        rows: 10,
        first: 0,
      });
      component.paginate('next');
      expect(component.turboTable.first).toEqual(10);
    });
    it('should not paginate previous if it is on first page', () => {
      spyOn(component, 'turboTable').and.returnValue({
        totalRecords: 20,
        rows: 10,
        first: 0,
      });
      component.paginate('previous');
      expect(component.turboTable.first).toEqual(0);
    });
    it('should not paginate next if it is on last page', () => {
      spyOn(component, 'turboTable').and.returnValue({
        totalRecords: 20,
        rows: 10,
        first: 10,
      });
      component.paginate('next');
      expect(component.turboTable.first).toEqual(10);
    });
  });

  describe('indexEditedFields', () => {
    it('should push unique indexes to the array', () => {
      const index = 1;
      component.options = { editedIndexes: [] };
      spyOn(component.indexArray, 'push');
      component.indexEditedFields(index);
      expect(component.indexArray.push).toHaveBeenCalledWith(index);
      expect(component.options.editedIndexes).toEqual(Array.from(new Set(component.indexArray)));
    });
  });

  describe('onRowChange', () => {
    it('should get selected index on select and emit', () => {
      const fakeEvent = new Event('FakeEvent');
      const fakeEventEmitter = new EventEmitter();
      component.records = [];
      component.options = { selectedIndex: 2 };
      component.onRow = fakeEventEmitter;
      spyOn(component.records, 'indexOf').and.returnValue(2);
      spyOn(component.onRow, 'emit');
      component.onRowChange(fakeEvent, 'select');
      expect(component.options.selectedIndex).toEqual(component.getSelectedIndex());
      expect(component.onRow.emit).toHaveBeenCalledWith(fakeEvent);
    });
    it('should set selected index to -1 and emit on unselect', () => {
      const fakeEvent = new Event('FakeEvent');
      const fakeEventEmitter = new EventEmitter();
      component.options = { selectedIndex: 2 };
      component.onRow = fakeEventEmitter;
      spyOn(component.onRow, 'emit');
      component.onRowChange(fakeEvent, 'unselect');
      expect(component.options.selectedIndex).toEqual(-1);
      expect(component.onRow.emit).toHaveBeenCalledWith(fakeEvent);
    });
  });
  describe('getSelectedIndex', () => {
    it('should return selected index', () => {
      component.records = [1, 2, 3];
      component.options.selection = 1;
      expect(component.getSelectedIndex()).toEqual(0);
    });
  });
});
