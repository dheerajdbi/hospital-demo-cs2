import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppConfiguration } from '../utils/app-configuration';

@Injectable()
export class RefreshService {

    constructor(private http: HttpClient, private appConfig: AppConfiguration) {
    }

    getPgmData(): Observable<any> {
        return this.http.get(this.appConfig.get('baseUrl') + 'refresh');
    }
}
