import { Injectable } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MessagesService {
    growlMessages: Message[] = [];
    growlMessageObservable: BehaviorSubject<Message[]> = new BehaviorSubject<Message[]>([]);

    //errMsgs = require('./messages-en.json');
    message: string;

    constructor() {
    }

    pushToMessages(MessageSeverity: string, MessageSummary: string, MessageDetail: string): void {
        this.growlMessages = [];
        this.growlMessages.push({ 'severity': MessageSeverity, 'summary': MessageSummary, 'detail': MessageDetail });
        this.growlMessageObservable.next(this.growlMessages);
    }

    clearMessages() {
        this.growlMessages = [];
        this.growlMessageObservable.next(this.growlMessages);
    }

    lookupAndShow(code: string): void {
        //this.message = this.errMsgs[code.toLowerCase()];
        if (this.message !== undefined && this.message !== '' && this.message !== null ) {
            this.pushToMessages('error', 'Error', this.message);
        }
    }
}
