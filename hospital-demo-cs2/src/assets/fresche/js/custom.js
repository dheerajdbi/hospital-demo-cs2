// site.js
// Russel Metals Inc. Metalware element control and event handling

(function($, window, undefined) {
    var supportPageOffset = window.pageXOffset !== undefined;
    var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");
    var scrollbarWidth = 0;

    function getXOffset() {
        return supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft;
    }

    function getYOffset() {
        return supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
    }

    /**
      External: jQuery UI getScrollParent function.
      @returns {HTMLElement} scroll parent
    */
    function getScrollParent(element, includeHidden) {
        var style = getComputedStyle(element);
        var excludeStaticParent = style.position === "absolute";
        var overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/;

        if (style.position === "fixed") return document.body;
        for (var parent = element;
            (parent = parent.parentElement);) {
            style = getComputedStyle(parent);
            if (excludeStaticParent && style.position === "static") {
                continue;
            }
            if (overflowRegex.test(style.overflow + style.overflowY + style.overflowX)) return parent;
        }

        return document.body;
    }

    /**
      Get scrollbar impact on viewport (some OS's have floating scrollbars)
    */
    function getScrollbarDimensions() {
        var $sbNode = $('<div style="width: 100px; height: 100px; ' +
            'overflow: scroll; position: absolute; top: -9999px">');

        $sbNode.appendTo('body');

        var sbNode = $sbNode.get(0);

        scrollbarWidth = sbNode.offsetWidth - sbNode.clientWidth;
    }

    /**
      Mock data source for tables
      @type object
    */
    var tableData = {};

    /**
      Mock Code data (keyed by table id)
      @type array
    */
    tableData.mocktable = [{
            "data": {
                "radio": {
                    "name": "mock",
                    "value": "01",
                    "id": "mock01",
                    "classlist": "c-form__Radio",
                },
                "label": {
                    "for": "mock01",
                    "text": "01",
                    "classlist": "c-table__Text h-payCode h-isSelectable"
                },
                "description": {
                    "text": "Mock description",
                    "classlist": "c-table__Text j-description"
                },
                "cells": [
                    { "class": "c-table__Data h-colCentred h-isSelectable" },
                    { "class": "c-table__Data" },
                    { "class": "c-table__Data" }
                ]
            }
        },
        {
            "data": {
                "radio": {
                    "name": "mock",
                    "value": "02",
                    "id": "mock02",
                    "classlist": "c-form__Radio",
                },
                "label": {
                    "for": "mock02",
                    "text": "02",
                    "classlist": "c-table__Text h-payCode h-isSelectable"
                },
                "description": {
                    "text": "Mock description",
                    "classlist": "c-table__Text j-description"
                },
                "cells": [
                    { "class": "c-table__Data h-colCentred h-isSelectable" },
                    { "class": "c-table__Data" },
                    { "class": "c-table__Data" }
                ]
            }
        },
        {
            "data": {
                "radio": {
                    "name": "mock",
                    "value": "03",
                    "id": "mock03",
                    "classlist": "c-form__Radio",
                },
                "label": {
                    "for": "mock03",
                    "text": "03",
                    "classlist": "c-table__Text h-payCode h-isSelectable"
                },
                "description": {
                    "text": "Mock description",
                    "classlist": "c-table__Text j-description"
                },
                "cells": [
                    { "class": "c-table__Data h-colCentred h-isSelectable" },
                    { "class": "c-table__Data" },
                    { "class": "c-table__Data" }
                ]
            }
        },
        {
            "data": {
                "radio": {
                    "name": "mock",
                    "value": "04",
                    "id": "mock04",
                    "classlist": "c-form__Radio",
                },
                "label": {
                    "for": "mock04",
                    "text": "04",
                    "classlist": "c-table__Text h-payCode h-isSelectable"
                },
                "description": {
                    "text": "Mock description",
                    "classlist": "c-table__Text j-description"
                },
                "cells": [
                    { "class": "c-table__Data h-colCentred h-isSelectable" },
                    { "class": "c-table__Data" },
                    { "class": "c-table__Data" }
                ]
            }
        },
        {
            "data": {
                "radio": {
                    "name": "mock",
                    "value": "05",
                    "id": "mock05",
                    "classlist": "c-form__Radio",
                },
                "label": {
                    "for": "mock05",
                    "text": "05",
                    "classlist": "c-table__Text h-payCode h-isSelectable"
                },
                "description": {
                    "text": "COD",
                    "classlist": "c-table__Text j-description"
                },
                "cells": [
                    { "class": "c-table__Data h-colCentred h-isSelectable" },
                    { "class": "c-table__Data" },
                    { "class": "c-table__Data" }
                ]
            }
        },
        {
            "data": {
                "radio": {
                    "name": "mock",
                    "value": "06",
                    "id": "mock06",
                    "classlist": "c-form__Radio",
                },
                "label": {
                    "for": "mock06",
                    "text": "06",
                    "classlist": "c-table__Text h-payCode h-isSelectable"
                },
                "description": {
                    "text": "Mock description",
                    "classlist": "c-table__Text j-description"
                },
                "cells": [
                    { "class": "c-table__Data h-colCentred h-isSelectable" },
                    { "class": "c-table__Data" },
                    { "class": "c-table__Data" }
                ]
            }
        }
    ];

    tableData.mocktable.columns = [{
            "render": renderRadioInput,
            "data": {},
            "createdCell": onCreatedCell,
            "width": "70px"
        },
        {
            "render": renderRadioLabel,
            "data": {},
            "createdCell": onCreatedCell,
            "width": "64px"
        },
        {
            "render": renderRadioDesc,
            "data": {},
            "createdCell": onCreatedCell,
            "width": "220px"
        }
    ];

    /**
      Tracking variable to store radio elements' selections.  DataTables
      does not track radio groups across pagination.  Therefore, we must do it
      ourselves. The upshot of this is any time the selected value is needed we
      need to get it from this variable and not from what we might get from
      someRadioElement.value
      @type object Keyed by form element name
    */
    var selectionValue = {};
    selectionValue.mock = "01";

    /**
      Set ellipsis decorator elements after elements marked as truncatable and
      verified to be cut short
    */
    function setTruncationDecorators() {
        var decorator = $('<div class="c-decorator">\u2026</div>');
        $('[class*=ellips]').each(function() {
            if ($(this).attr('style')) {
                $(this).attr('style', $(this).attr('style')
                    .match(/(.*(?:^|[; ]))width:[^;]+(?:[; ]|$)(.*)/)
                    .slice(1).join(''));
            }

            var curDim = $(this).get(0).getBoundingClientRect(), // get actual bounds
                str = $(this).text(),
                testEl = $(this).clone(),
                steps = [];

            var testDim = testEl.css({
                    //        'visibility': 'hidden',           // hide from viewer
                    'top': '125px', // debug
                    'left': '150px', // debug
                    'font-size': $(this).css('font-size'),
                    'font-family': $(this).css('font-family'),
                    'font-weight': $(this).css('font-weight'),
                    'position': 'absolute', // remove from document flow
                    'width': 'auto' // let expand to content
                }).attr('id', 'testForWidth') // set unique id
                .appendTo('body') // append to document
                .get(0).getBoundingClientRect(); // access node and get bounds

            for (var i = str.length; i > 1; i--) { // > 1 because substring(0,0) is empty
                var test = str.substring(0, i - 1);
                var w = testEl.text(test).get(0).getBoundingClientRect().width;
                if (w !== steps[steps.length - 1]) steps.push(w);
            }

            testEl.remove(); // remove from DOM

            $(this).siblings('.c-decorator').remove();
            if (curDim.width < testDim.width) {
                var bestFit = null;
                for (i = 0; i < steps.length; i++) {
                    if (steps[i] <= curDim.width) {
                        bestFit = steps[i];
                        break;
                    }
                }

                if (bestFit !== null) {
                    // set element width to not cut text if possible
                    $(this).css('width', bestFit + 'px');
                }

                // element is truncated, apply decorator
                $(this).after(decorator.clone());
            }
        });
    }

    /**
      Set delegate handler for table truncated elements' tooltips
    */
    function setTruncatedElTooltips() {
        var templateStr = '<div class="c-tooltip" role="tooltip">' +
            '<div class="c-tooltip__Arrow"></div>' +
            '<div class="c-tooltip__Inner"></div></div>';

        function setTooltipTitle() {
            return $(this).text();
        }

        function showTooltip(e) {
            var target;

            if (e.type === 'mouseenter') target = e.currentTarget;
            else if (e.type === 'focusin') target = e.target;
            else return;

            if ($(target).next().hasClass('c-tooltip')) {
                $(target).next().stop().fadeIn();
            } else {
                var newTip = $(templateStr).insertAfter(target);
                newTip.find('.c-tooltip__Inner')
                    .text($.proxy(setTooltipTitle, target));

                var sizeClone = newTip.clone();
                var naturalDim = sizeClone.appendTo('body').show().get(0)
                    .getBoundingClientRect();
                sizeClone.remove();

                var targetDim = $(target).offset();
                targetDim.bottom = targetDim.top + $(target).height();

                // position above if we'll overflow the viewport
                var nTip = newTip.get(0),
                    tipScrollParent = getScrollParent(nTip),
                    sPBottom = $(tipScrollParent).offset().top +
                    $(tipScrollParent).height();

                if (targetDim.bottom + naturalDim.height > sPBottom) {
                    newTip.css('top', '-100%');
                    var arrow = newTip.find('.c-tooltip__Arrow');
                    arrow.css({
                        'top': 'initial',
                        'bottom': '-' + arrow.css('height'),
                        'border-bottom-color': 'transparent',
                        'border-top-color': newTip.css('background-color')
                    });
                }

                newTip.css('width', naturalDim.width + 'px');
                newTip.stop().fadeIn();
            }
        }

        function hideTooltip(e) {
            var target;

            if (e.type === 'mouseleave' || e.type === 'mousedown') {
                target = e.currentTarget;
            } else if (e.type === 'focusout') {
                target = e.target;
            } else {
                return;
            }

            if ($(target).next().hasClass('c-tooltip')) {
                $(target).next().stop().fadeOut('fast');
            } else {
                return;
            }
        }

        var classSelector = '[class*=ellips]';
        // $('body')
        //   .on('mouseenter', classSelector, showTooltip)
        //   .on('focusin', classSelector, showTooltip)
        //   .on('mousedown', classSelector, hideTooltip)
        //   .on('mouseleave', classSelector, hideTooltip)
        //   .on('focusout', classSelector, hideTooltip);
    }

    /**
      Callback to render radio button in datatables cell
    */
    function renderRadioInput(data, type, full, meta) {
        var rData = full.radio;
        var checked = selectionValue[rData.name] === rData.value ? 'checked' : '';

        return '<input class="' + rData.classlist + '" type="radio" ' +
            'name="' + rData.name + '" value="' + rData.value + '" id="' + rData.id +
            '" ' + checked + '><div class="c-form__Graphic--radio" tabindex="' +
            '0"></div>';
    }

    /**
      Callback to render label for form element in datatables cell
    */
    function renderRadioLabel(data, type, full, meta) {
        var lData = full.label;
        var classes = lData.classlist;
        if (selectionValue[full.radio.name] === full.radio.value) {
            classes += " h-isSelected"
        }

        return '<label class="' + classes + '" for="' + lData.for+'">' +
            lData.text + '</label>';
    }

    /**
      Callback to render descriptive text in datatables cell
    */
    function renderRadioDesc(data, type, full, meta) {
        var dData = full.description;
        var classes = dData.classlist;
        if (selectionValue[full.radio.name] === full.radio.value) {
            classes += " h-isSelected"
        }

        return '<p class="' + classes + '">' + dData.text + '</p>';
    }

    /**
      Callback hook into datatables API after table cell has been created
    */
    function onCreatedCell(cell, cellData, rowData, rowIndex, colIndex) {
        var $cell = $(cell),
            classlist = rowData.cells[colIndex].class;
        $cell.addClass(classlist);

        if ($cell.find('input[type=radio]:checked').length ||
            $cell.find('.h-isSelected').length) {
            var newClass = "h-isSelected__Cell";
            if (colIndex === 0) {
                newClass += "--first";
            } else if (colIndex === rowData.cells.length - 1) {
                newClass += "--last";
            }

            $cell.addClass(newClass);
        }
    }

    /**
      Callback hook into datatables API after table row has been created
    */
    function onCreatedRow(row, data, dataIndex) {
        var $row = $(row),
            classList = "c-table__Row h-isSelectable";

        $row.addClass(classList);

        if ($row.find('[class*=h-isSelected]').length) {
            $row.addClass('h-isSelected');
        }
    }

    /**
      Helper function for paging lifted from the datatables input pagination
      plugin
    */
    function calcCurrentPage(oSettings) {
        return Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
    }
    /**
      Another helper function from the datatables input pagination plugin
    */
    function calcPages(oSettings) {
        return Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength);
    }

    /**
      Helper function from input pagination to work out what can be disabled
    */
    function calcDisableClasses(oSettings) {
        var start = oSettings._iDisplayStart;
        var length = oSettings._iDisplayLength;
        var visibleRecords = oSettings.fnRecordsDisplay();
        var all = length === -1;

        // Gordey Doronin: Re-used this code from main jQuery.dataTables source
        // code. To be consistent.
        var page = all ? 0 : Math.ceil(start / length);
        var pages = all ? 1 : Math.ceil(visibleRecords / length);

        var disableFirstPrevClass = (page > 0 ? '' : oSettings.oClasses.sPageButtonDisabled);
        var disableNextLastClass = (page < pages - 1 ? '' : oSettings.oClasses.sPageButtonDisabled);

        return {
            'previous': disableFirstPrevClass,
            'next': disableNextLastClass,
        };
    }

    /**
      Russel Metals custom pagination extension
      Built off of the datatables input pagination and four button navigation
      plugins' code
    */
    if (typeof $.fn.DataTable !== "undefined") {
        $.fn.dataTableExt.oPagination.rmi = {
            "fnInit": function(oSettings, nPaging, fnCallbackDraw) {
                var nPrevious = document.createElement('span'),
                    nNext = document.createElement('span'),
                    nNoRecords = document.createElement('span'),
                    langDefaults = oSettings.oLanguage.oPaginate,
                    classes = oSettings.oClasses;

                nNoRecords.appendChild(document.createTextNode('No More Records'));
                nPrevious.appendChild(document.createTextNode('View Previous Records'));
                nNext.appendChild(document.createTextNode('View More Records'));

                nNoRecords.className = "c-table__Pagination--none";

                nPrevious.className = "c-table__Pagination--prev" + " " +
                    classes.sPageButton;
                nNext.className = "c-table__Pagination--next" + " " +
                    classes.sPageButton;

                nPrevious.setAttribute('tabindex', '0');
                nNext.setAttribute('tabindex', '0');

                if (oSettings.sTableId !== '') {
                    nPaging.setAttribute('id', oSettings.sTableId + '__Pagination');
                    nNoRecords.setAttribute('id', oSettings.sTableId +
                        '__Pagination--none');
                    nPrevious.setAttribute('id', oSettings.sTableId +
                        '__Pagination--prev');
                    nNext.setAttribute('id', oSettings.sTableId + '__Pagination--next');
                }

                nPaging.appendChild(nNoRecords);
                nPaging.appendChild(nPrevious);
                nPaging.appendChild(nNext);

                $(nPrevious).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== 1) {
                        oSettings.oApi._fnPageChange(oSettings, 'previous');
                        fnCallbackDraw(oSettings);
                    }
                });

                $(nNext).click(function() {
                    var iCurrentPage = calcCurrentPage(oSettings);
                    if (iCurrentPage !== calcPages(oSettings)) {
                        oSettings.oApi._fnPageChange(oSettings, 'next');
                        fnCallbackDraw(oSettings);
                    }
                });

                if (calcPages(oSettings) <= 1) {
                    // hide paging when we can't page
                    $(nPrevious, nNext).css('visibility', 'hidden');
                    $(nNoRecords).show();
                } else {
                    $(nPrevious, nNext).css('visibility', 'visible');
                    $(nNoRecords).hide();
                }
            },
            "fnUpdate": function(oSettings) {
                if (!oSettings.aanFeatures.p) return;

                var an = oSettings.aanFeatures.p,
                    iPages = calcPages(oSettings),
                    iCurrentPage = calcCurrentPage(oSettings),
                    prevNext = $(an).find('.c-table__Pagination--prev,' +
                        '.c-table__Pagination--next'),
                    nNoRecords = $(an).find('.c-table__Pagination--none');

                if (iPages <= 1) { // hide paging when we can't page
                    prevNext.css('visibility', 'hidden');
                    nNoRecords.show();
                    return;
                }

                var disableClasses = calcDisableClasses(oSettings);

                prevNext.css('visibility', 'visible');
                nNoRecords.hide();

                // Enable/Disable "prev" button
                $(an).children('.c-table__Pagination--prev')
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses['previous']);

                // Enable/Disable "next" button
                $(an).children('.c-table__Pagination--next')
                    .removeClass(oSettings.oClasses.sPageButtonDisabled)
                    .addClass(disableClasses['next']);
            }
        };
    }

    /**
      Activate any datatables on the page
      @returns undefined
    */
    function activateDataTables() {
        // if DataTable functions loaded
        if (typeof $.fn.DataTable !== 'undefined') {
            $('.j-datatable').each(function() {
                var tableId = $(this).attr('id'),
                    tableOpts = {};

                if (tableId in tableData &&
                    typeof tableData[tableId] !== 'undefined') {

                    // map the input objects' data properties onto the datatables data
                    // array
                    tableOpts.data = tableData[tableId].map(function(cV, idx, arr) {
                        return cV.data;
                    }, tableData[tableId]);

                    if ('columns' in tableData[tableId] &&
                        typeof tableData[tableId].columns !== 'undefined') {
                        // define column rendering, set callback to add cell classes
                        tableOpts.columns = tableData[tableId].columns;
                    }
                }

                // set callback to add in row classes
                tableOpts.createdRow = onCreatedRow;

                // set draw callback to handle update of radio selection, truncation
                // see comments for selectionValue above, also
                tableOpts.drawCallback = function(oSettings) {
                    var api = new $.fn.dataTable.Api(oSettings);

                    setTruncationDecorators();

                    $.each(selectionValue, function(prop, value) {
                        var selector = 'input[type=radio][name=' + prop + '][value!=' +
                            value + ']';

                        $(api.table().node()).find(selector)
                            .prop('checked', false)
                            .each(function() {
                                var row = $(this).closest('tr');

                                row.add(row.find('[class*=h-isSelected]'))
                                    .removeClass('h-isSelected h-isSelected__Cell ' +
                                        'h-isSelected__Cell--first h-isSelected__Cell--last'
                                    );
                            });
                    });
                };

                tableOpts.dom = "<f'row'<'col-xs-12'l>>" +
                    "<'row'<'col-xs-12'tr>>" +
                    "<'row'<'col-xs-12'p>>";
                tableOpts.colReorder = true;
                tableOpts.columnDefs = [ 
                    { targets: 0, searchable: false },
                    { targets: [1,2], searchable: true },
                    { targets: '_all', searchable: false }
                ];
                $(this).DataTable(tableOpts);
            });
        }
    }

    /**
      Modal popups can have tabular displays.  Trigger radio button when row
      is clicked, and apply appropriate style classes
    */
    function setSelectableRowListener() {
        $('body').on('click', '.j-listener--rows tr', function(e) {
            var row = $(this),
                parent = row.closest('tbody'),
                radio = row.find('input[type=radio]');;

            // check radio input in this row
            radio.prop('checked', true);

            selectionValue[radio.attr('name')] = radio.attr('value');

            // remove previous selected elements' style classes
            parent.find('[class*=h-isSelected]').removeClass(
                "h-isSelected h-isSelected__Cell h-isSelected__Cell--first " +
                "h-isSelected__Cell--last"
            );

            // add selected style elements to current row
            row.addClass('h-isSelected')
                .find('td')
                .eq(0).addClass('h-isSelected__Cell--first').end()
                .eq(-1).addClass('h-isSelected__Cell--last').end()
                .slice(1, -1).addClass('h-isSelected__Cell').end()
                .find('[class*=c-table__Text]').addClass('h-isSelected');

            setTruncationDecorators(); // re-calc ellipsis
            updateFieldFromForm(radio.closest('form').get(0));
        });
    }

    /**
      Display the modal passed as a parameter
      @param {object} modal jQuery object containing modal element reference
      @param {object} trigger jQuery object containing trigger element reference
    */
    function showModal(modal, trigger) {
        // Set body classes (bootstrap's modal-open, and our scrollbar helper)
        var body = $('body').get(0),
            bodyClasses = 'modal-open';
        if (scrollbarWidth > 0 && body.scrollHeight > body.clientHeight) {
            // scrollbar takes up non-zero space in window flow and the page has one
            // so add our 'unshift' class
            bodyClasses += ' h-scrollXShift';
        }
        $('body').addClass(bodyClasses);

        // Show modal and lightbox
        $('.c-modal__Lightbox').add(modal).show();
        // Redraw datatable (header row needs refresh once table has 'flow')
        modal.addClass('j-isOpen').find('.j-datatable').DataTable().draw();

        // Try to accommodate fitting the modal on screen, centred vertically
        // on the launching element if possible.

        // Start with some calculated values.
        // tGroup/gOffset added later, refactor would likely eliminate trigger
        // offset vars
        var tGroup = trigger.closest('.c-form__LookupGroup'), // for width
            gOffset = tGroup.offset(), // group
            tOffset = trigger.offset(), // trigger
            mOffset = modal.offset(), // modal

            // vertical centre of modal
            mHalfHeight = Math.floor((modal.get(0).clientHeight / 2) + 0.5),
            // height of trigger
            tHeight = trigger.get(0).clientHeight,
            // width of trigger
            tWidth = trigger.get(0).clientWidth,
            // vertical centre of trigger
            tHalfHeight = Math.floor((tHeight / 2) + 0.5),
            // page vertical offset
            yOff = getYOffset(),
            // modal top position in order to be vertically centred on trigger
            mNewTop = (tOffset.top + tHeight) - yOff - mHalfHeight,
            // page bottom (zero'd, not accounting offset as we took it out of mNewTop)
            yBottom = document.documentElement.clientHeight,
            // modal bottom
            mNewBottom = mNewTop + (mHalfHeight * 2);

        gOffset.right = gOffset.left + tGroup.get(0).clientWidth;
        gOffset.bottom = gOffset.top + tGroup.get(0).clientHeight;
        mOffset.right = mOffset.left + modal.get(0).clientWidth;

        function placeModal(overlap) {
            if (overlap) {
                if ((tOffset.top + tHalfHeight) > (yBottom / 2)) {
                    // element is in the lower half of the screen, place modal above
                    mNewTop = tOffset.top - (mHalfHeight * 2) - 30 - yOff;

                    // set arrow position
                    modal.addClass('c-modal__Arrow--bottom');
                } else {
                    // element is in the upper half of the screen, place modal below
                    mNewTop = tOffset.top + tHeight + 30 + yOff;

                    // set arrow position
                    modal.addClass('c-modal__Arrow--top');
                }
            } else {
                if (mNewTop < 0) {
                    mNewTop = 0;
                } else if (mNewBottom > yBottom) {
                    var delta = mNewBottom - yBottom,
                        mLessDelta = mNewTop - delta;

                    if (mLessDelta >= 0) {
                        mNewTop = mLessDelta;
                    } else {
                        // hey, we tried
                        // alternative option here would be to force shrink the modal
                        mNewTop = 0; // get as much on screen as possible
                    }
                }
            }
        }

        // Where horizontally are we in relation to our trigger group?
        // Modals are set up to be able to use the bootstrap grid for column
        // alignment so we need to see are we left/right or over?
        // Use the trigger group dimensions (gOffset)
        if (mOffset.left > gOffset.right || mOffset.right < gOffset.left) {
            // Modal is to the right or left of the trigger group
            // ASSUMES: Horizontal element position is within screen dimensions
            // since this is done manually and in the bootstrap grid.
            // This code does not adjust the horizontal, only the vertical
            placeModal(false);
        } else {
            // Modal overlaps the trigger group
            placeModal(true);
        }

        // set the final top position
        modal.css({
            'top': mNewTop + 'px'
        });

        modal.find('[class^=c-trailGuide]').focus(); // grab the focus
    }

    /**
      Locate and hide the open modal, reset body classes
    */
    function hideModal() {
        $('[class*=h-isActive]').removeClass("h-isActive__Field h-isActive__Label" +
            " h-isActive__TextInput h-isActive__LookupButton" +
            " h-isActive__LookupQuery");
        $('.j-isOpen[class*=c-modal]').add('.c-modal__Lightbox').hide();
        $('body').removeClass('modal-open h-scrollXShift');
    }

    /**
      Handle clicks on the lightbox background (as a close action)
    */
    function setLightboxListener() {
        // uncomment if you'd like clicking on the lightbox (the gray shadow around
        // a modal element) to close the modal and return you to the regular page
        //    $('.c-modal__Lightbox').on('click', function (e) {
        //      if( e.target == this ) {
        //        hideModal();
        //      }
        //    });
    }

    /**
      Set handlers for button click events that trigger modal popups
    */
    function setModalTriggerListeners() {
        $('.c-form__LookupButton, .c-form__LookupQuery').on('click', function(e) {
            var trigger = $(this),
                triggerGroup = trigger.closest('.c-form__LookupGroup'),
                parentGroup = trigger.closest('.c-form__Field'),
                fieldId = triggerGroup.prev('input').attr('id'),
                modalId = fieldId + 'Modal',
                modal = $('#' + modalId);

            if (modal.length) {
                // set trigger field elements to highlighted state
                parentGroup.addClass('h-isActive__Field')
                    .find('.c-form__Label').addClass('h-isActive__Label').end()
                    .find('.c-form__Text').addClass('h-isActive__TextInput').end()
                    .find('.c-form__LookupButton')
                    .addClass('h-isActive__LookupButton').end()
                    .find('.c-form__LookupQuery').addClass('h-isActive__LookupQuery');

                showModal(modal, trigger);
            }
        });

        // 'previous' link
        $('.c-trailGuide__Back--popup').on('click', function(e) {
            e.preventDefault(); // this link does not trigger a page load

            // hide modal and reset trigger elements to regular state
            hideModal();
        });

        // 'continue' button
        $('[class^=c-modal]').find('form').on('submit', function(e) {
            e.preventDefault(); // no page reload, please


            updateFieldFromForm(this);
            hideModal();
        });

        // try to keep focus within the modal
        $('[class^=c-modal]').not('.c-modal__Panel,.c-modal__Lightbox')
            .on('focusout', function(e) {
                console.log(e.relatedTarget);
                if (e.relatedTarget !== null &&
                    !$(this).find(e.relatedTarget).length) {
                    var modal = $(this);
                    e.stopPropagation();
                    setTimeout(function() {
                        modal.find('[class^=c-trailGuide]').first().focus();
                    }, 100);
                }
            });
        $('body').on('focusin', function(e) {
            var modal = $('.j-isOpen');
            if (modal.length && !modal.find(e.target).length) {
                setTimeout(function() {
                    modal.find('[class^=c-trailGuide]').first().focus();
                }, 100);
            }
        });
    }

    /**
      Using the passed in form element, find and update the associated non-modal
      form's entry.
    */
    function updateFieldFromForm(form) {
        // get associated form field (outside modal)
        var inField = $(form).closest('[class^=c-modal]');
        if (!inField.length) return;

        var inputField = $('#' + inField.attr('id').match(/^(.+)Modal$/)[1]);

        // and associated query display (if any)
        var queryField = inputField.next('.c-form__LookupGroup')
            .find('.c-form__LookupQuery');

        // get the radio selection field name
        var radioName = $(form).find('input[type=radio]').attr('name');

        // verify stored selection value exists
        if (typeof selectionValue[radioName] !== 'undefined') {
            inputField.val(selectionValue[radioName]);

            if (queryField.length) {
                var desc = $(form).find('#' + radioName + selectionValue[radioName])
                    .closest('.c-table__Row').find('.j-description');

                if (desc.length) {
                    queryField.text(desc.text());
                }
            }
        }
    }

    /**
      Set handlers for dropdown list click events. Set active style classes
      and update display accordingly.
    */
    function setDropdownSelectListeners() {
        $('.j-selectList .c-list__Item').on('click', function(e) {
            // $('.c-button_Dropdown').on('click', function(e) {
            $(this)
                .siblings().removeClass('h-isSelected__ListItem').end()
                .addClass('h-isSelected__ListItem')
                .closest('.j-selectList')
                .find('[class*=c-button__Dropdown]')
                .find('.j-innerText')
                .text($(this).text());
        });
    }

    /**
      Set handler for clicks on filters
    */
    function setFilterTriggerListeners() {
        $('.c-button__Filter').on('click', function(e) {
            $(this).siblings().removeClass('h-isSelected__Button');
            $(this).toggleClass('h-isSelected__Button');

            // call specific filter handler here
            $(this).trigger('click.rmi'); // trigger further actions
        });
    }

    /**
      Set handling of key presses on our custom radio graphic element
    */
    function setRadioGraphicKeyListeners() {
        $('body').on('keyup', '.c-form__Graphic--radio', function(e) {
            var keyCode = ('key' in e) ? e.key : ('which' in e) ? e.which : e.keyCode;

            var radioEl = $(this).prev();

            // handle space
            if (keyCode === " " || keyCode === 32) {
                radioEl.prop('checked', true); // radio doesn't toggle off, only on

                // check if we're part of a table and trigger the row handler if so
                var row = radioEl.closest('tr');

                if (row.length) {
                    // is it part of a listener group?
                    if (row.closest('.j-listener--rows').length) {
                        row.trigger('click');
                    }
                }
            }
        });
    }

    /**
      Some radios have labels as the only visibly targettable element for keyboard
      access
    */
    function setRadioLabelKeyListeners() {
        $('body').on('keyup', '.c-form__Label--inlineRadio', function(e) {
            var keyCode = ('key' in e) ? e.key : ('which' in e) ? e.which : e.keyCode,
                radioEl = $('#' + $(this).attr('for'));

            // handle space
            if (keyCode === " " || keyCode === 32) {
                radioEl.prop('checked', true); // radio doesn't toggle off, only on
            }
        });
    }

    /**
      Set handling of key presses when focus is on a dropdown item
    */
    function setDropdownKeyListeners() {
        $('body').on('keyup', '.c-list__Text', function(e) {
            var keyCode = ('key' in e) ? e.key : ('which' in e) ? e.which : e.keyCode;

            // handle space
            if (keyCode === " " || keyCode === 32) {
                $(this).closest('.dropdown').find('.c-button__Dropdown')
                    .trigger('click').focus();
            }
            // handle enter
            else if (keyCode === "Enter" || keyCode === 13) {
                $(this).trigger('click');
            }
        });
    }

    /**
      Set handling of key presses when focus is on table pagination elements
    */
    function setPagingKeyListeners() {
        function handlePageKeyPress(e) {
            var keyCode = ('key' in e) ? e.key : ('which' in e) ? e.which : e.keyCode;

            // handle enter
            if (keyCode === "Enter" || keyCode === 13) {
                $(this).trigger('click');
            }
        }

        $('body').on('keyup', '.c-table__Pagination--prev', handlePageKeyPress);
        $('body').on('keyup', '.c-table__Pagination--next', handlePageKeyPress);
    }

    function demoFilterActingOnTable() {
        if ('dataTable' in $.fn) {
            $.fn.dataTableExt.afnFiltering.push(
                function(settings, searchData, index, rowData, counter) {
                    if (typeof window.RMIFilter !== 'undefined' &&
                        window.RMIFilter === 'thickness') {
                        return index < 10; // match the first 10 rows only
                    }

                    return true; // if filter isn't active, always match
                }
            );

            $('#filterThickness').on('click.rmi', function(e) {
                if ($(this).hasClass('h-isSelected__Button')) {
                    window.RMIFilter = "thickness";
                } else {
                    window.RMIFilter = null;
                }

                setTimeout(function() {
                    $('.j-datatable').DataTable().draw();
                }, 100);
            });
            $('.c-button__Filter').not('#filterThickness').on('click.rmi',
                function(e) {
                    window.RMIFilter = null;
                    setTimeout(function() {
                        $('.j-datatable').DataTable().draw();
                    }, 100);
                });
        }
    }

    $(document).ready(function() {
        getScrollbarDimensions();
        activateDataTables();
        setSelectableRowListener();
        setLightboxListener();
        setModalTriggerListeners();
        setTruncationDecorators();
        setTruncatedElTooltips();
        setDropdownSelectListeners();
        setFilterTriggerListeners();
        setRadioGraphicKeyListeners();
        setRadioLabelKeyListeners();
        setDropdownKeyListeners();
        setPagingKeyListeners();
        demoFilterActingOnTable();
    });

})(jQuery, window);